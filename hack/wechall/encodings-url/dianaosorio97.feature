## Version 2.0
## language: en


Feature: Training, Encoding-WeChalll
  Site:
    WeChalll
  Category:
    Encoding
  User:
    dianaosorio97

  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |  18.04.1    |
    | Mozilla Firefox |  63.0       |
  Machine information:
    Given I am accessing the challenge page
    And I see the problem statement
    """
    Your task is to decode the following:
    %59%69%70%70%65%68%21%20%59%6F%75%72%20%55%52%4C%20%69%
    73%20%63%68%61%6C%6C%65%6E%67%65%2F%74%72%61%69%6E%69%6
    E%67%2F%65%6E%63%6F%64%69%6E%67%73%2F%75%72%6C%2F%73%61
    %77%5F%6C%6F%74%69%6F%6E%2E%70%68%70%3F%70%3D%62%61%6C%
    67%72%73%67%67%66%62%6F%69%26%63%69%64%3D%35%32%23%70%6
    1%73%73%77%6F%72%64%3D%66%69%62%72%65%5F%6F%70%74%69%63
    %73%20%56%65%72%79%20%77%65%6C%6C%20%64%6F%6E%65%21
    """
    Then I read information about Encoding in python
    And I see that use a library urllib.parse unquote


  Scenario: Successful solution
    Given the url
    Then I can use unquote that replace special characters
    Then I write the program in python
    And I get the decoded url
    Then I put url in the browser
    And I solve the challenge
