## Version 2.0
## language: en

Feature: sidology-WeChall
  Site:
    WeChall
  Category:
    Audio
  User:
    dianaosorio97
  Goal:
    Find the name of the games

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Mozilla Firefox | 63.0        |
    | Audacious       | 3.9         |
    | App Google      | 8.33.6.21   |

  Machine information:
    Given I am accessing the challenge page
    And I see the problem statement
    """
    As you are a quite a good hacker you should
    have no problem to gather information.
    So please tell me the name of these games,
    where I only have the .sid files from.
    """
    Then I download the compressed file
    And I read information about files .sid

  Scenario: Fail:Searching the name by characters
    Given The audio files
    Then I download the tool Audacious 3.9
    And I Listen the first audio
    Then I can see the name of first game in the playlist
    Then I Listen the second audio
    And I see the final characters of the author's name
    Then I look for commodore 64 music authors
    Then I read author's name
    Then I looked for the name that had the same characters at the end
    And I found the name of the second game
    Then I listen to the third audio
    And I count the characters of the name
    Given I have the number of characters of the name
    Then I looked the sounds with the same number of characters
    And I listened to the songs
    But I did not find the same audio

  Scenario: Successful solution
    Given the third audio
    Then I use google audio search
    And I found the game name
    And I found the name of the games
    Then I write the three names of the games
    And I solved the challenge
