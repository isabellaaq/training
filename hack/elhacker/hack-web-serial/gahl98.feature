## Version 2.0
## language: en

Feature: serial hackweb elhacker
  Code:
    3
  Site:
    http://warzone.elhacker.net/
  Category:
    hackweb
  User:
    gahl98
  Goal:
    Find a valid serial

  Background:
    Hacker's software:
      | <Software name> | <Version>    |
      | Zorin OS        | 12.4         |
      | Chromium        | 73.0.3683.86 |

  Machine information:
    Given the challenge URL
    """
    http://warzone.elhacker.net/pruebas.php?p=3
    """
    Then I opened the URL with Chromium
    And I see the challenge description
    """
    Tienes que sacar un serial correcto
    """
    And I see a link
    """
    Entrar
    """
    When I click the link
    Then the page is load whith a new URL
    """
    http://warzone.elhacker.net/web/codeme.html
    """
    And I see the challenge statement
    """
    Tu mision en el dia de hoy no es nada mas que entrar al area restringida de
    Zona 0. Como recuerdan estamos en una Zona de Guerra y tenemos que atacar a
    los enemigos. Se dice que hay una informacion que nos dara una gran ventaja
    en esta batalla. Nuestros espias nunca se equivocan y por eso necesitamos
    que tu entres a este lugar y nos saque la informacion. El problema es que
    solamente permiten a personas autorizadas con un codigo especial y un
    codigo solo sirve para una vez. Asi que intenta de entrar a la area
    restringida con algun codigo no usado previamente.
    """
    And I see the input field to submit the answer
    """
    Codigo de Acceso:
    """
    And I see a button
    """
    Entrar
    """
    When I click the button
    Then an alert box is displayed
    """
    Codigo No Valido
    """
    And I close the alert box
    And I don't write nothing in the input field to submit the answer
    But I conclude that I must see the URL's source code

  Scenario: Sucess:running script of page
    Given the page of the statement
    """
    http://warzone.elhacker.net/web/codeme.html
    """
    Then I open Chromium developer tools
    And I see the URL's source code
    And I see a script loading in the body before the form
    And I go to the tab sources in DevTools
    And I see the javascript code
    And I see that it verify that the imput code is 16 long
    And I see it splits the code in 2 substrings
    And I see the first keep the last char
    And I see the second keep the first 15 chars
    And I see it call a function over that substrings
    And I see that the function verify if every char is a number se
    And I see that after that it starts to make operations with the substring
    And I think that it could assign a letter at the end of process
    And I search for that assigment
    And I dont find it
    But I find it compares the result of operations with the first substring
    Then I copy that portion of the script to an html source code as script
    And I asign a value to the second substring
    """
    111111111111111
    """
    And I assign output of operations by console
    Then I open the html in Chromium
    And I open the DevTools
    And I see in console tab the result
    """
    8
    """
    Then I write in the input field of form
    """
    1111111111111118
    """
    And I push the button
    """
    Entrar
    """
    Then it loads a new page
    """
    http://warzone.elhacker.net/web/ver.php?cob=1111111111111118
    """
    And I see an ending challenge statement
    """
    Pass correcto, este es tu código de verificacion, guardalo
    7b6243e306e0de01f73670b50119fa94
    """
    And I see a link
    """
    Meter mi código
    """
    Then I click it and load a new page
    """
    http://warzone.elhacker.net/lvl.php?p=3&x=9e66c5d34d5376599490e878d43502f6
    """
    And I see it ask to evaluate the challenge
