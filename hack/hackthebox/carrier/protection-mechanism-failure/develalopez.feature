# language: en

Feature:
  TOE:
    HTB Carrier Machine
  Page name:
    Hack The Box
  CWE:
    CWE-693: Protection Mechanism Failure
  Goal:
    Getting user.txt flag and claiming user ownership of the machine.
  Recommendation:
    Check for bad user input in decrypted check parameter.

  Background:
  Hacker's software:
    | <Name>       | <Version>    |
    | Kali Linux   | 3.30.1       |
    | Firefox ESR  | 60.2.0esr    |
    | Burpsuite CE | 1.7.36       |
    | OpenVPN      | 2.4.6        |
    | nmap         | 7.70         |
    | metasploit   | 14.17.17-dev |
  TOE information:
    Given that I use OpenVPN to find the machine
    And I am accessing the site 10.10.10.105
    And enter a php site that shows a login form
    And PHP version 7.0
    And OpenSSH version 7.6
    And is running on Ubuntu 4 with kernel Linux
    And runs Apache 2.4.18


  Scenario:
    Given I access the IP
    Then I can see a logo image
    And two error codes: 45007 and 45009
    And a login form
    When I access as admin
    Then I see a diagnostics site
    And a button that says Verify status

  Scenario: Static detection
  The php site admits Base64 encripted command injection.
    When I access to the site as admin
    And I look at the HTML code of the site
    Then I can see the vulnerability caused by a form from line 42 to line 49
    """
    42  <form role="form" method="post">
    43    <input type="hidden" id="check" name="check" value="cXVhZ2dh">
    44    ...
    49  </form>
    """
    Then I check if the value of check is encrypted
    And the result of Base64 decryption is the word quagga
    Then I try and encrypt the following command:
    """
    root | ls
    """
    And I see that the website shows me the contents of a folder
    Then I can conclude that I can find user.txt by command injection

  Scenario: Dynamic detection
  The php site admits Base64 encripted command injection.
    Given that I press the Verify status button
    And I intercept the request with Burp
    Then I can see the following HTTP header:
    """
    POST /diag.php HTTP/1.1
    Host: 10.10.10.105
    ...
    Upgrade-Insecure-Requests: 1

    check=cXVhZ2dh
    """
    Given that the Upgrade-Insecure-Requests header is 1
    Then I assume that the check value is encrypted in some form
    And when I check Base64 it returns the word quagga
    Then I check if I can inject encrypted commands in check
    And they return their expected output
    Then I can conclude that I can find user.txt by command injection

  Scenario: Exploitation
    Given I can inject Base64 encrypted commands
    And see the output in the site
    Then I can encrypt and execute the following command:
    """
    root | ls
    """
    Then I get the output:
    """
    ...
    user.txt
    ...
    """
    And I can conclude that read the user.txt flag.

  Scenario Outline: Extraction
    Given I can inject Base64 encrypted commands
    And see the output in the site
    Then I can encrypt and execute the following command:
    """
    root | cat user.txt
    """
    Then I get the user flag
    And I can conclude that I can claim user ownership

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:C/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  systems/hackthebox-carrier/insuff-protected-credentials
    Given I accessed the site as admin provided by insuff-protected-credentials
    Then I can exploit this vulnerability
    Then I get user.txt
