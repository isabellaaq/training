## Version 2.0
## language: en

Feature: Emdee_five_for_life-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    web
  User:
    felipetic
  Goal:
    Find the flag on the given web

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali-Linux      | 2019.3    |
    | Firefox-Quantum | 60.8.0esr |
    | Python          | 2.7.16+   |

  Scenario: Fail:ctf
    Given the website, I open it
    Then only task was to submit the string after converting it to MD5 hash
    And when I tried to submit I got this, Too slow!
    Then I realized that was other choice

  Scenario: Success:ctf
    Given the website, I open it
    """
    https://docker.hackthebox.eu:34561
    """
    Then I analyzed the source code
    And I build a python script, I automated this
    Then of I built the Python script [evidence](emdeefive.py)
    And I ran it
    """
    root@Kraken:~/Escritorio# python emdeefive.py
    """
    Then I get the output from source code
    """
    <html>
      <head>
         <title>emdee five for life</title>
      </head>
      <body style="background-color:powderblue;">
            <h1 align='center'></h1><p align='center'>HTB{N1c3_ScrIpt1nG_B0i!}
            <input type="text" name="hash" align='center'></input>
            </br>
            <input type="submit" value="Submit"></input>
            </form></center>
      </body>
    </html>
    """
    And I got the flag
    And I solved the flag challenge

