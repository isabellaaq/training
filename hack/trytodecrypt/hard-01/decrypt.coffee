
char0 = '0'.charCodeAt(0)
chara = 'a'.charCodeAt(0)
charA = 'A'.charCodeAt(0)
other_chars = '-_.,;:?! '

decrypt = (chcode,key)->
  console.log(chcode+' '+key)
  switch
    when chcode >= key && chcode <= key+9
      char = String.fromCharCode(char0+chcode-key)
    when chcode >= key+10 && chcode <= key+35
      char = String.fromCharCode(chara+chcode-key-10)
    when chcode >= key+36 && chcode <= key+61
      char = String.fromCharCode(charA+chcode-key-36)
    when chcode > key+61
      char = other_chars[chcode-key-62]

main = ()->
  text = '59656A6B6F9F656A67746767'
  parts = text.match(/.{1,2}/g)
  r = ''
  key = parseInt(parts[0],16)
  for i in [1...parts.length]
    r += decrypt(parseInt(parts[i],16),key)
  console.log(r)

main()
