## Version 1.0
## language: en

Feature: 1-cryptography-listbrain
   Site:
    listbrain.awardspace.biz
  Category:
    cryptography
  User:
    lekriss1234
  Goal:
    Discover the decrypted text

  Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    http://listbrain.awardspace.biz/?p=CR003
    """
    Then I open the URL with Mozilla
    And I see the sample case
    """
    testing <-> hsehwbu
    """
    Then I see the challenge
    """
    ecgofse <-> ???????
    """

  Scenario: Success: replacing the given letters in the challenge statement
    Given I replaced in ecgofse the letters traducted in
    """
    testing <-> hsehwbu
    """
    Then I obtain the following statement
    """
    scgofes
    """
    Then I put the resulting statement in the website "https://quipqiup.com/"
    Then I obtain a long list of possible solutions
    And I get interested in the following three of them
    """
    sources
    samples
    squares
    """
    Then I prove them in that order
    Then I see that the third one is the correct
    Then I captured the flag
    And I solved the challenge
