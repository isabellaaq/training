# language: en

Feature: Solve challenge 46
  From site W3Challs
  From Criptography Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using python Python 2.7.14+
  And I am using VIM - Vi IMproved 8.0
  And I am running Burp Suite Community edition v1.7.30
  Given a web site stating there is a communication between two parties
  """
  URL: https://w3challs.com/challs/Crypto/diffie_hellman/
  challenge_diffie_hellman.php
  Message: Diffie-Hellman
  Details: Alice and Bob, uses Diffie Hellman to exchange a secret key
  over an insecure channel without authentication then Alice will send
  a code encrypted by a xor with the secret key.
  Objective: Get the code
  Evidence: - Php site
            - p, g values
            - Exchange simulation with editable parameters
            - Less than 30 seconds to answer each stage
  """

Scenario Outline: Diffie-Hellman brief explanation
Without digging to much in Diffie-Hellman theory and as stated before
There is a key exchange between two parties. How it works?
  Given P, G values, publicly known
  And where P is a prime number
  And G is a primitive root of P
  And where G < P
  """
  # Example: Publicly know values
  Prime number P = 353
  Primiteve root G = 3

  Fullfils G < P
  """

  Given two users, <Alice> and <Bob>
  And both selects a random private key
  And <Alice> key is Xa
  And <Bob> key is Xb
  """
  # Example: Random private key selection
  Xa = 97
  Xb = 233
  """

  Given both private keys, calculate for each party, their public keys
  And the computation follows this equation <g^Xa mod P>
  And <Alice> public key is Ya
  And <Bob> public key is Yb
  """
  # Example: Public key generation
  Ya = 3^97 mod 353   = 40
  Yb = 3^233 mod 353  = 248
  """
  Then <Alice> sends her public key to <Bob>
  And <Bob> replies with his public key to <Alice>

  Given both private and public keys, compute the secret key
  And the computation for each secret key follows this formula:
  """
  # Explanation & Example: Secret key generation
  # Alice secret key
  Ka = (Yb)^Xa mod P
  Ka = 248^97 mod 353 = 160

  # Bob secret key
  Kb = (Ya)^Xb mod P
  Kb = 40^233 mod 353 = 160
  """
  Then both have exchanged a secret value
  And now they can use it as an encryption key
  And an attacker would only known P, G, Ya and Yb values
  But computing the private key for each is not feasible
  And thanks discrete logarithm for that
  """
  # Example: Discrete log attack
  Xa = dlog[G, P](Yb)
  Which it must not be feasible with big digits
  """

Scenario Outline: Diffie-Hellman MITM
As stated before in te challenge details: The communication is
Performed in an insecure channel without authentication, which
Is indeed prone to man-in-the-middle attacks, as there is no
Real authentication on who is who.
  Given the two users at start of the exchange
  Then we introduce a new unknown party, lets call it <D.>
  """
  # Prime P & Primitive root G, publicly known
  P = 353
  G = 3
  """
  Then <D.> the attacker sits in the middle of the exchange
  And selects a private key, Xd1, Xd2
  Then computes a public key, Yd1, Yd2
  """
  # Example: D. select private key
  Xd1 = 7
  Xd2 = 6

  # Compuing Yd1
  Yd1 = 3^7 mod 353   = 69
  Yd2 = 3^6 mod 353   = 23
  """
  Then <Alice> and <Bob> select their private keys
  And both, computes their public keys
  """
  # Alice and Bob private keys
  Xa = 97
  Xb = 233

  # Alice and Bob public key generation
  Ya = 3^97 mod 353   = 40
  Yb = 3^233 mod 353  = 248
  """

  Then <Alice> start communication by transmiting to <Bob> her public key, Ya
  And <D.> intercepts Ya
  And transmits Yd1 to <Bob>
  Then <D.> calculates a secret key, K2:
  """
  # Secret key calculation
  K2 = (Ya)^Xd2 mod P = 153
  """

  Then <Bob> receives Yd1
  And calculates his secret key, K1:
  """
  # Secret key calculation
  K1 = (Yd1)^Xb mod P = 125
  """

  Then <Bob> transmits Yb to <Alice>
  And <D.> intercepts Yb
  And transmits Yd2 to <Alice>
  But <D.> calculates another secret key, K1:
  """
  # Secret key calculation
  K1 = (Yb)^Xd1 mod P = 125
  """

  Then <Alice> receives Yd2
  And calculates her secret key, K2:
  """
  # Secret key calculation
  K2 = (Yd2)^Xa mod P = 153
  """

  Then <Alice> and <Bob> think that they share a secret key
  But the reallity is that <Alice> shares a secret key with <D.>, K2
  And <Bob> shares antoher secret key with <D.>, K1
  And now all future communication is compromised

Scenario: Exploitation
  Given the libraries <beautifulSoup> and <requests> from python
  Then I code a <connect> function:
  """
  Source: w3challs/challenge46/skhorn.py
  31  def connect(self, url, cookie, payload=""):
  36    response = requests.post(url,
  37                           cookies=cookie,
  38                           data=payload,
  39                           verify=False).text
  40    return response
  """

  And I use it, to retrieve data from the web page
  But I need to prepare connection parameters:
  """
  Source: w3challs/challenge46/skhorn.py
  8  URL = [ 'https://w3challs.com/challs/Crypto/diffie_hellman/
  "challenge_diffie_hellman.php', ... ]
  16  COOKIES = { cookies } => Dict session cookies
  """

  And I perform the first request
  Then I get as a response, the entire html of the challenge
  And this data contains the initial values, as P, G
  """
  # Get p,g values
  Source: w3challs/challenge46/skhorn.py
  81  html_search = search_in_html(response, 'textarea')
  82
  83  for item in html_search:
  84      items_array = item.text.split()
  85
  86  prime_q = int(items_array[2])
  87  alpha = int(items_array[5])
  """

  And the start of the exchange with <Alice> sending Ya to <Bob>
  And I retrieve this value
  """
  # Getting Ya from <Alice>
  Source: w3challs/challenge46/skhorn.py
  93     response = connect(URL[1], COOKIES, payload)
  94     html_search = search_in_html(response, 'textarea')
  95     for item in html_search:
  96         items_array = item.text.split()
  97
  99     alice_key = items_array[len(items_array)-1]
  100    alice_key = int(alice_key[:-1])
  """

  Then I select my random private keys
  """
  # Private keys selection
  Source: w3challs/challenge46/skhorn.py
  75     Xd1 = 97
  76     Xd2 = 233
  """

  And I compute Yd1
  And send it to <Bob> instead of Ya
  """
  # Computing Yd1 and Send it to Bob
  Source: w3challs/challenge46/skhorn.py
  107     Yd1 = compute_key(alpha, Xd1, prime_q)
  117     items_array[len(items_array)-1] = str(Yd1)
  118     concat_message = ' '.join(items_array)
  119     concat_message += '"'
  ...
  127     url_concat = URL[1]+"?type="+DHKEY_TYPE[0]
  128     payload = {'alice_send_key': concat_message}
  132     response = connect(url_concat, COOKIES, payload)
  """

  And I calculate K2
  """
  Source: w3challs/challenge46/skhorn.py
  113     K2 = compute_key(alice_key, Xd2, prime_q)
  """

  Then <Bob> transmits Yb to <Alice>
  And guess who intercept it?
  Then I retrieve Yb, to send Yd2
  """
  # Retrieving Bob's public key
  Source: w3challs/challenge46/skhorn.py
  133     html_search = search_in_html(response, 'textarea')
  134     for item in html_search:
  135         items_array = item.text.split()
  139     bob_key = items_array[len(items_array)-1]
  140     bob_key = int(bob_key[:-1])
  142
  """
  And I calculate Yd2
  And the other secret key, K2
  """
  Source: w3challs/challenge46/skhorn.py
  147     Yd2 = compute_key(alpha, Xd2, prime_q)
  ...
  152     K1 = compute_key(alice_key, Xd2, prime_q)
  """

  Then I transmit Yd2 to <Alice>
  """
  # Sending Yd2 to Alice
  Source: w3challs/challenge46/skhorn.py
  156     items_array[len(items_array)-1] = str(Yd2)
  157     concat_message = ' '.join(items_array)
  158     concat_message += '"'
  ...
  164     url_concat = URL[1]+"?type="+DHKEY_TYPE[1]
  165     payload = {'bob_send_key': concat_message}
  166
  167     response = connect(url_concat, COOKIES, payload)
  """

  And as response I get the code, xored with their secret key
  """
  # Retrieving code from response
  Source: w3challs/challenge46/skhorn.py
  169     html_search = search_in_html(response, 'textarea')
  170     for items in html_search:
  171         items_array = items.text.split()
  172
  174     crypted_code = items_array[len(items_array)-1]
  175     crypted_code = crypted_code[:-1]
  """

  Then I make a xor, with each secret key generated by me
  """
  Source: w3challs/challenge46/skhorn.py
  178     xor_key_1 = xor_them(int(crypted_code), int(K2))
  ...
  180     xor_key_2 = xor_them(int(crypted_code), int(K1))
  """

  And I send the decrypted code via GET request
  """
  183     url_concat = URL[2]
  184     print(url_concat)
  185     payload = {'password': xor_key_1}
  186
  187     res = requests.get(url_concat, cookies=COOKIES, params=payload)
  """

  Then I pass the challenge
  """
  <title>Diffie-Hellman Challenge</title>
  </head>
  <body>Congraluations, the password to validate this challenge is ...
  </html>
  """
