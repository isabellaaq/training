## language: en

Feature: Solve the challenge
  CTF site:
    www.root-me.org
  Category:
    Network
  Page name:
    TELNET - authentication
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.04.1 LTS (amd64)       |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
  CTF site information:
    Given I'm at the challenge page
    And the statement says
    """
    Find the user password in this TELNET session capture.
    """
    And there is a wireshark's packet capture (.pcap) file

  Scenario: Capturing the flag
  Finding the password and solving the challenge
    When I open the .pcap file with wireshark
    Then I see the captured packages [evidence](image1.png)
    When I click on follow the TCP stream on wireshark
    Then a new window is displayed [evidence](image2.png)
    And I see the flag is "user"
    When enter the flag as solution
    Then I solve the challenge
