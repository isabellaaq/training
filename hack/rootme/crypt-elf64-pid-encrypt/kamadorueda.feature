## Version 2.0
## language: en

Feature: Root Me-Crypto-ELF64 - PID encryption
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    ELF64 - PID encryption
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                               |
    | Ubuntu          | 18.04.1 LTS (amd64)                     |
    | GNU bash        | 4.4.19(1)-release (x86_64-pc-linux-gnu) |
    | Google Chrome   | 70.0.3538.77 (64-bit)                   |

  Machine information:
    Given I'm accesing the challenge page
    And a Clang source code is provided
    """
    01 /*
    02  * gcc ch21.c -lcrypt -o ch21
    03  */
    04
    05 #include <stdio.h>
    06 #include <stdlib.h>
    07 #include <string.h>
    08 #include <crypt.h>
    09 #include <sys/types.h>
    10 #include <unistd.h>
    11
    12 int main (int argc, char *argv[]) {
    13     char pid[16];
    14     char *args[] = { "/bin/bash", "-p", 0 };
    15
    16     snprintf(pid, sizeof(pid), "%i", getpid());
    17     if (argc != 2)
    18         return 0;
    19
    20     printf("%s=%s",argv[1], crypt(pid, "$1$awesome"));
    21
    22     if (strcmp(argv[1], crypt(pid, "$1$awesome")) == 0) {
    23         printf("WIN!\n");
    24         execve(args[0], &args[0], NULL);
    26     } else {
    27         printf("Fail... :/\n");
    28     }
    29     return 0;
    30 }
    """
    And an SSH access is provided
    """
    $ ssh -p 2221 cryptanalyse-ch21@challenge01.root-me.org
    Username: cryptanalyse-ch21
    Password: cryptanalyse-ch21
    """

  Scenario: Fail: recoinaisance
    When I ssh into the server
    Then I am prompted into a command line
    """
    $ whoami
      cryptanalyse-ch21
    """
    When I list files in current directory
    # I've replaced the following to make the output shorter:
    #   A="cryptanalyse-ch21"
    #   B="cryptanalyse-ch21-cracked"
    """
    total 32
    drwxr-s---  3 ${A}         ${A}         4096 Oct 31 04:09 .
    drwxr-s--x 44 challenge    www-data     4096 Oct 21 15:49 ..
    -r--------  1 ${B}         ${B}           14 Aug 11  2015 .passwd
    -rwsr-x---  1 ${B}         ${A}         8952 Mar  4  2018 ch21
    -r--r-----  1 ${B}         ${A}          591 Mar  4  2018 ch21.c
    """
    Then I conclude I'm able to run ./ch21 with my permissions
    And I conclude I'm not able to read ./.passwd with my permissions
    And I conclude that I need to elevate my privileges to "${B}"

  Scenario: Fail: running ch21
    When I look at the Clang code
    """
    17     if (argc != 2)
    18         return 0;
    """
    Then I conclude I have to pass ./ch21 a single parameter or it'll close
    When I execute some commands sucessively
    Then I get
    """
    $ ./ch21 kevin
      kevin=$1$awesome$N.dkBPjibti2EwBjpAUt70Fail... :/
    $ ./ch21 kevin
      kevin=$1$awesome$pjpzOr1PhiK/7AD/zIumg0Fail... :/
    $ ./ch21 kevin
      kevin=$1$awesome$AKFpt7vqJw78ByLxRgi.X1Fail... :/
    """

  Scenario: Fail: analizing the code
    When I look at the relevant Clang code lines
    """
    13     char pid[16];
    16     snprintf(pid, sizeof(pid), "%i", getpid());
    22     if (strcmp(argv[1], crypt(pid, "$1$awesome")) == 0) {
    23         printf("WIN!\n");
    24         execve(args[0], &args[0], NULL);
    26     } else {
    27         printf("Fail... :/\n");
    28     }
    """
    Then I write a easier to understand pseudocode
    """
    01 get the current process ID with getpid()
    02 cast the process ID (integer) into an array of char called 'pid'
    03 compare argv[1] with crypt(pid, "$1$awesome")
    04 if argv[1] == crypt(pid, "$1$awesome"):
    05   exec a command line with elevated privileges
    06 else:
    07   fail the challenge, and exit the program
    """
    And I see I can exploit the process ID assignment logic

  Scenario: Fail: analyzing the process ID assignment logic
    Given I create a payload that extract the current process ID
    """
    pid=$(ps|grep "ps"|awk -F' ' '{print $1}');echo ${pid}
    """
    When I execute it multiple times
    Then I see the process ID increments in four each time [evidence](img1.png)
    # The explanation of why four is:
    # ps   gets assigned the current process ID
    # grep gets assigned the next process ID
    # awk  gets assigned the next process ID
    # echo gets assigned the next process ID
    And I conclude that the process ID is assigned sequentially

  Scenario: Fail: creating a code to print the hash of a given PID
    Given I have a one-line script to print the current process ID
    """
    pid=$(ps|grep "ps"|awk -F' ' '{print $1}');echo ${pid}
    """
    When I run this script on the server
    Then I get
    """
    22846
    """
    Given I create a Clang code to see the hash of the PID 24000
    # because it is near to 22846
    """
    01 #include <stdio.h>
    02 #include <crypt.h>
    03 int main () {
    04     int pid = 24000;
    05     char pidc[16];
    06     snprintf(pidc, sizeof(pidc), "%i", pid);
    07     printf("%i = %s\n", pid, crypt(pidc, "$1$awesome"));
    08     return 0;
    09 }
    """
    When I run this Clang code on my local machine
    Then I get
    """
    24000 = $1$awesome$HmiHW7fdPEBBvqTsa6wyc0
    """
    Given I create a one-line script to run the ./ch21 file a thousand times
    And it has injected the hash of the process ID 24000 as first parameter
    """
    for i in {1..1000}; do ./ch21 '$1$awesome$HmiHW7fdPEBBvqTsa6wyc0'; done
    """
    When I run this code in the server
    Then I get a terminal with elevated privileges [evidence](img2.png)
    """
    bash-4.3$ whoami
      cryptanalyse-ch21-cracked
    """
    When I read the file ./.passwd
    Then I get the flag [evidence](img3.png)
    """
    -/q2/a9d6e31D
    """
    And I solve the challenge [evidence](img4.png)
