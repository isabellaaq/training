/*
'tslint --init' # init linter configuration
'tslint henrymej.ts' # apply linter
tsc henrymej.ts # compile ts into js
*/

const files = require("fs");
const path = require("path");

function solven(n) {
  const m = BigInt(1000000000);
  const result = [BigInt(1)];
  let k = 1;
  while (k < n + 1) {
    result.push((BigInt(n - k + 1) * BigInt(result[k - 1])) / BigInt(k));
    k += 1;
  }
  k = 0;
  while (k < n + 1) {
    result[k] = result[k] % m;
    k += 1;
  }
  return result;
}

function solve(n, k) {
  return solven(n + k - 1)[k];
}

files.readFile(path.join(__dirname, "DATA.lst"), (flaw, stream) => {
  if (flaw) {
    throw flaw;
  }
  const inputs = stream.toString().split("\n");
  const [ total ] = inputs;
  let outputs = "";
  for (let index = 1; index <= total * 2; index += 2) {
    const base = 10;
    const n = parseInt(inputs[index], base);
    const k = parseInt(inputs[index + 1], base);
    const outp = solve(n, k);
    outputs = `${ outputs } ${ outp }`;
  }
  outputs = outputs.substring(1, outputs.length);
  console.log(outputs);
});

/*
node henrymej.js
344234994 577336240 579607360 885019500 679172000
32895360 418607600 774492320 713132000 775651600
*/
