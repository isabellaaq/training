
(comment
lein eastwood #linting
)

(require '[clojure.string :as str])
(def inputs (str/split (slurp "DATA.lst") #"\n"))
(defn finder [arr x y]
  (cond
    (> x y) "Odd"
    :else
      (case (clojure.core/mod(get arr (- x 1)) 2)
        1 "Odd"
        (cond (<= (+ x 1) y)
          (case (get arr x)
            0 "Odd"
            "Even"
          )
          :else "Even"
        )
      )
  )
)

(defn solve [arr queries](
  into [] (
    for [q queries] (finder arr (get q 0) (get q 1))
    )
  )
)

(def q (Integer/parseInt (str/trim (get inputs 0))))

(def arr (vec (map #(Integer/parseInt %) (
  clojure.string/split (get inputs 1) #" "))))

(def queries [])
(def i 3)
(doseq [_ (range q)]
    (def queries (conj queries (vec (
        map #(Integer/parseInt %) (
            clojure.string/split (
                get inputs i) #" ")))))
    (def i (inc i))
)

(def result (str/join " " (vec (solve arr queries))))

(println result)

(comment
clojure henrymej.clj
Odd Odd Odd Even Odd Even Odd Odd Even Odd
)
