"""
    Solution to RangeAdditionII problem
    >pylint wgiraldom1.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""
import json


def max_count(matrix_width, matrix_height, ops):
    """
    Perform the intersection algorithm
    """
    min_x = matrix_width
    min_y = matrix_height
    if ops:
        for operation in ops:
            if operation[0] < min_x:
                min_x = operation[0]
            if operation[1] < min_y:
                min_y = operation[1]
    return min_x*min_y


CONTENT = json.loads(open("DATA.lst").read())
for element in CONTENT:
    print max_count(element[0], element[1], element[2])
#
#     >python wgiraldom1.py
#     4
#     4
#     1
