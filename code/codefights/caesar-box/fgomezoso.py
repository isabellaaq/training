# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program encodes a message using the caesar box transposition cipher.
The string must be broken in a square and then its read column by column
"""
from __future__ import print_function
import math


def caesar_box_cipher_encoding(string_input):

    """
    The string is split in n parts using the square root of the string's
    length, and every column is build with loops
    """

    slen = len(string_input)

    pos = int(math.sqrt(slen))

    list1 = []
    list2 = []

    for i in range(0, slen, pos):

        list1.append(string_input[i:i+pos])

    for i in range(0, pos):

        mystr = ""

        for k in range(0, pos):

            mystr += list1[k][i]

        list2.append(mystr)

    message = ''.join(list2)

    return message


def main():

    """
    Reads the input data and uses the caesar box function
    """
    data = open("DATA.lst", "r")

    string = data.readline().rstrip('\n')

    encoded_message = caesar_box_cipher_encoding(string)

    print(encoded_message)

    data.close()


main()

# $ python fgomezoso.py
# seerietsxnt1tle6
