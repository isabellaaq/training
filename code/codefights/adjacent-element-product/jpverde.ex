# $ mix credo
# Analysis took 0.07 seconds (0.01s to load, 0.06s running checks)
# 4 mods/funs, found no issues.

defmodule Product do
  @moduledoc """
    Solution to codefights problem adjacentElementProduct
  """
  def data do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    _dat = nums |> Enum.map(&String.to_integer/1)
  end

  def result do
  inputArray = Product.data
  product = fn(list) -> Enum.reduce(list,  &*/2) end
  _val =
      inputArray
      |> Enum.chunk_every(2,1, :discard)
      |> Enum.map(product)
      |> Enum.max
  end
end

# $ iex
# iex(1)> c("jpverde.ex")
# iex(2)> Product.result
# 21
