# $ mix credo
#   Analysis took 0.1 seconds (0.03s to load, 0.07s running checks)
#   4 mods/funs, found no issues.

defmodule Century do
  @moduledoc """
    Solution to codefights problem centuryFromYear
  """
  def data do
    {:ok, contents} = File.read("DATA.lst")
    _date =
      contents
      |> String.split(["\r\n"], trim: true)
      |> List.to_string
      |> String.to_integer
  end

  def result do
    year = Century.data

    if rem(year, 100) > 0 do
      cent = (year / 100) + 1 |> Kernel.trunc
      IO.puts(cent)
    else
      cent = year / 100 |> Kernel.trunc
      IO.puts(cent)
    end
  end
  :ok
end

# $ iex
# iex(1)> c("jpverde.ex")
# iex(2)> Century.result
# 20
# :ok
