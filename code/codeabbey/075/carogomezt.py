import math


def game(pair, three, four, yacht, y):

    if ((pair == 0) and (three == 0) and (four == 0) and (yacht == 0)):
        cont = 0
        for i in range(len(y)-1):
            if (int(y[i]) == int(y[i+1]) - 1):
                cont += 1
            else:
                break
        if cont == 4:
            if (y[0] == "1"):
                return "small-straight"
            elif (y[0] == "2"):
                return "big-straight"
            else:
                return "none"
    else:
        if pair == 2:
            return "two-pairs"
        elif (pair == 1) and (three == 1):
            return "full-house"
        elif three == 1:
            return "three"
        elif four == 1:
            return "four"
        elif yacht == 1:
            return "yacht"
        elif pair == 1:
            return "pair"
        else:
            return "none"

def main():
    n = int(input())
    s = ""
    while(n > 0):
        val = input()
        val = val.split(" ")
        y = []
        for i in val:
            y.append(i)
        y.sort()
        cont = 0
        result = [0, 0, 0, 0, 0, 0]
        for i in range(len(y)-1):
            if (y[i] == y[i+1]):
                cont += 1
                result[int(y[i]) - 1] = cont
            else:
                cont = 0
        result.sort(reverse=True)
        pair = 0
        three = 0
        four = 0
        yacht = 0

        for i in result:
            if i == 1:
                pair += 1
            elif i == 2:
                three += 1
            elif i == 3:
                four += 1
            elif i == 4:
                yacht += 1
            else:
                continue

        r = game(pair, three, four, yacht, y)
        s = s + " " + str(r).lower()
        n -= 1
    s = s[1:len(s)]
    print (s)

if __name__ == "__main__":
    main()
