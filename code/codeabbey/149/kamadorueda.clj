;; $ lein eastwood
;;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;;   Directories scanned for source files:
;;     src test
;;   == Linting kamadoatfluid.clj ==
;;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

;; namespace
(ns kamadoatfluid.clj
  (:gen-class))

;; casts a string into a number
(defn toint [s]
  (Integer/parseInt s))

;; casts a number into a string
(defn tostr [n]
  (loop [s (str n)]
    (if (= (count s) 5)
      s
      (recur (str "0" s)))))

;; load lines of file handled by 'rdr' into a vector of integers
(defn loadf [rdr]
  (vec (map toint (line-seq rdr))))

;; returns true if 'n' is a prime number
(defn prime? [n]
  (let [d (range 2 (inc (int (Math/sqrt n))))
        r (map #(mod n %) d)]
          (not-any? #(= % 0) r)))

;; returns true if both contain 'yyyy', "a: xyyyy" == "b: yyyyz"
(defn chain? [a b]
    (= (subs a 1 5) (subs b 0 4)))

;; returns a list with primes not great than 'upto'
(defn gen-primes [upto]
  (map tostr (take upto (filter prime? (iterate inc 2)))))

;; returns a list of elements chainable to 'e'
(defn get-chainables [lp e]
  (map #(subs % 4 5) (filter #(chain? e %) lp)))

;; returns a hash-map data structure 'end_of_chain -> posible_next_lien'
(defn create-map [lp]
  (zipmap (map #(subs % 1 5) lp) (map #(get-chainables lp %) lp)))

;; returns the first character of a string as an int
(defn first-c [s]
  (toint (subs s 0 1)))

;; returns everything but the first character of a string as a string
(defn no-first-c [s]
  (subs s 1))

;; return the codeabbey's hash of a string
(defn hashit [chain]
  (loop [hash 0 rc chain]
    (if (empty? rc)
      hash
      (recur (mod (+ (* hash 13) (first-c rc)) 100000007) (no-first-c rc)))))

;; add a new lien to the chain, if possible. (non-deterministic)
(defn append-val [chain ds]
  (let [cnt  (count chain)
        key  (subs chain (- cnt 4))
        val  (vec (get ds key))
        nval (count val)
        rnd  (rand-int nval)]
    (if (= 0 nval)
      (if (< cnt 5)
        chain
        (subs chain 0 (- cnt 1)))
      (str chain (get val rnd)))))

;; iteratively seed, construct, and hash a chain until hash == ohash
(defn solve [ohash]
  (let [lp (vec (gen-primes 9592))
        ds (create-map lp)]
    (loop []
      (let [rnd   (rand-int 9592)
            seed  (get lp rnd)
            chain (last (take 100 (iterate #(append-val % ds) seed)))
            sz    (count chain)]
        (if (and (>= sz 48) (= (hashit chain) ohash))
          chain
          (recur))))))

;; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (let [f      (loadf rdr)
          ntests (get f 0)]
      (dotimes [i ntests]
        (let [ohash (get f (+ i 1))]
          (println "searching for hash: " ohash)
          (println "|-- " (solve ohash)))))))

;; fire it ma'am!
(defn -main [& args]
  (process_file "DATA.lst"))

;; $lein run
;; searching for hash:  55626666
;; |-- 74873133391139313913931393139313331193913931333179
;; searching for hash:  25234357
;; |-- 94331991393179397919391393139139313331139313913933
;; searching for hash:  55882915
;; |-- 9499391393133391193913931391393133317939979319913933
;; searching for hash:  77732498
;; |-- 940499391193913931793979193911393139139313931793979
