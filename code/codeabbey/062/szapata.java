/*
$java -jar checkstyle-8.23-all.jar -c /google_checks.xml ./szapata.java
Starting audit...
[WARN] /szapata.java:26:14: Type name 'szapata' must
match pattern '^[A-Z][a-zA-Z0-9]*$'. [TypeName]
Audit done.

$javac ../NetBeansProjects/testJava/src/main/java/fluid/codeabbey62/szapata.java
*/

package fluid.codeabbey62;

import java.util.Scanner;


/**
 * find the number of primes between give ranges.
 *
 * <p>.
 *
 * @author sebastian
 */
public class szapata {

  /**
   * stores array with true when the number is positive
   *
   * <p>.
  */
  public static boolean[] primeNumbers;


  /**
   * application method.
   *
   * <p>aplication runner
   *
   * @param args args
  */

  public static void main(String []args) {


    int[][] coupleLimitations;
    int maxPrime = 0;
    Scanner in = new Scanner(System.in);
    int numberOfCouples = in.nextInt();
    coupleLimitations = new int[2][numberOfCouples];
    for (int i = 0;i < coupleLimitations[0].length;i++) {
      coupleLimitations[0][i] = in.nextInt();
      coupleLimitations[1][i] = in.nextInt();
      if (coupleLimitations[0][i] > maxPrime) {
        maxPrime = coupleLimitations[0][i];
      }
      if (coupleLimitations[1][i] > maxPrime) {
        maxPrime = coupleLimitations[1][i];
      }
    }
    findPrimesFrom0to(maxPrime);
    //go through the couples finding the limits
    for (int i = 0;i < numberOfCouples;i++) {
      int primesInThisRange = 0;
      for (int start = coupleLimitations[0][i]
          ;start <= coupleLimitations[1][i];start++) {
        if (primeNumbers[start]) {
          primesInThisRange++;
        }

      }
      System.out.print(primesInThisRange + " ");
    }

  }

  /**
  * storages prime number to the top.
  *
  * <p>.
  *
  * @param maxNumber > 0, max number to where the primes would be needed
  */
  public static void findPrimesFrom0to(int maxNumber) {
    primeNumbers = new boolean[maxNumber + 1];

    for (int i = 1;i <= maxNumber;i = i + 2) {
      primeNumbers[i] = isPrime(i);
    }
    primeNumbers[2] = true;
  }

  /**
  * allow to determine if a given number is prime.
  *
  * <p>.
  *
  * @param n number to check if is prime
  * @return is it prime?
  */
  public static boolean isPrime(int n) {
    int top = n;
    if (n > 20) {
      top = (int) (Math.sqrt(n) + 1);
    }
    if (n == 1) {
      return false;
    }
    for (int i = 3;i < top;i = i + 2) {
      if (n % i == 0) {
        return false;
      }
    }

    return true;
  }
}

/*
$java fluid.codeabbey62.szapata
83240 8887 14177 7452 51121 4484 11804 31344 2119 60923 17235 27382 11202
38837 28516 43885 25869 5028 49326 9581 32738 3878 16131 535 54619 3369 5562
*/

