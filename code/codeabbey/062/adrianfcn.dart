/*
 $ dartanalyzer adrianfcn.dart
Analyzing 62.dart...
No issues found!
*/
import 'dart:math' as math;
import 'dart:io';
import "dart:convert";

List list_primes(int n) {
  List natureNumber = [];
  for (var i = 2; i < n + 1; i++) {
    natureNumber.add(i);
  }
  for (int i in natureNumber) {
    int vl = i;
    if (math.pow(vl, 2) <= n - 2 && vl != 0) {
      for (int j = 2; j < (n / vl) + 1; j++) {
        if (((vl * j) - 2) < n - 1) {
          natureNumber[(vl * j) - 2] = 0;
        }
      }
    }
  }
  natureNumber.insert(0, 0);
  natureNumber.insert(1, 0);
  return natureNumber;
}

int primes_range(int lower_limit, int upper_limit, List list) {
  var sol = 0;
  for (int i = lower_limit; i < upper_limit + 1; i++) {
    if (list[i] != 0) {
      sol++;
    }
  }
  return sol;
}

void main() {
  List list = list_primes(3000000);
  final List<String> source = File("DATA.lst").readAsStringSync(encoding: utf8)
    .split("\n");
  String sol = "";
  for(int i = 1;i < source.length - 1; i++) {
    var limits = List(2);
    var ad = source[i].split(" ");
    int j = 0;
    for(var a in ad ) {
      limits[j] = int.parse(a);
      j++;
    }
  sol += primes_range(limits[0], limits[1], list).toString() + " ";
  }
  print(sol);
}
/*
  $ dart adrianfcn.dart
  83240 8887 14177 7452 51121 4484 11804 31344 2119 60923 17235 27382 11202
  38837 28516 43885 25869 5028 49326 9581 32738 3878 16131 535 54619 3369 5562
*/
