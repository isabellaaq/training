/**
 * $ dartanalyzer sebasbeleno.dart
 * Analyzing  sebasleno.dart
 * No issues found!
 */
import 'dart:io';
import 'package:path/path.dart' show dirname, join;

void main() {
  var file = File(join(dirname(Platform.script.path), 'DATA.lst'));

  file.readAsLines().then((List data) {
    var testCases = int.parse(data[0]);
    var answers = [];

    for (var i = 0; i < testCases; i++) {
      var strword = data[i + 1];
      var wordsWithOutSpaces = strword.replaceAll(RegExp(r'\W'), '');
      wordsWithOutSpaces.toLowerCase();
      var words = wordsWithOutSpaces.split('');
      var reverseWords = getReverseWords(words);
      var isPalindrome = evaluateWords(words, reverseWords);

      if (isPalindrome) {
        answers.add('Y');
      }
      else {
        answers.add('N');
      }
    }

    for (var answer in answers) {
      print(answer);
    }
  });
}

List getReverseWords(words) {
  var reverseWords = [];
  for (var i = words.length - 1; i >= 0; i--) {
    reverseWords.add(words[i]);
  }

  return reverseWords;
}

bool evaluateWords(words, reverseWords) {
  var aux = 0;
  for (var i = 0; i < words.length; i++) {
    if (words[i] == reverseWords[i]) aux++;
  }
  if (aux == words.length) return true;

  return false;
}

/**
 * dart sebasbeleno.dart
 * N
 * Y
 * Y
 */
