(comment "
  $ lein eastwood
  == Eastwood 0.2.8 Clojure 1.8.0 JVM 10.0.2
  Directories scanned for source files:
  src
  == Linting tizcano.core ==
  == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0")

(ns tizcano.core
  (:gen-class))

(defn factorial [n]
  (reduce *' (range 1 (inc n))))

(defn binomial-coefficient
  [n k]
  (/ (/ (factorial n)
        (factorial k))
     (factorial (- n k))))

(defn catalan-numbers
  [n]
  (/ (binomial-coefficient (* 2 n) n) (+ n 1)))

(defn -main
  [& args]
  (print (str (catalan-numbers (Long/parseLong (read-line))))))

(comment "
  $ lein run
  input.
  242
  output.
  745083048705701371739175488430123074761356441724465872216047559413072301222
9779734483705845264372088386793917715649471951423459864067454911200")
