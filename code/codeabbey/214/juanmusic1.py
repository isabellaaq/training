#!/usr/bin/env python
'''
$ pylint juanmusic1.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''
import copy
X = []


def read_data(file1):
    '''Returns splited data'''
    data_raw = open(file1, "r")
    if data_raw.mode == 'r':
        contents = data_raw.read()
        data_raw.close()
    x_1 = contents.splitlines()
    data = []
    for ind in x_1:
        data.append(ind.split())
    num = data.pop(0)
    num = num.pop()
    for ind1 in data:
        for j in ind1:
            if "-" in j:
                ind1[ind1.index(j)] = j.split('-')
    data.sort()
    return (data, num)


def get_common(x_1, city_pointer, number):
    '''Returns a list of people in a city in common '''
    common = reset_common(number)
    index = 0
    count = 0
    # Saca ciudad de cada persona
    for data, pointer in zip(x_1, city_pointer):
        city = data[2][pointer]
        count = count + 1
        index = 0
        for data_1, pointer_1 in zip(x_1, city_pointer):
            if city == data_1[2][pointer_1]:
                if common[index] == 0:
                    common[index] = count
            index = index + 1
    return common


def reset_common(number):
    '''Returns common_list with zeros '''
    temp_list = []
    ind = 0
    while ind < int(number):
        ind = ind + 1
        temp_list.append(0)
    return temp_list


def refresh_health(x_1, in_common_1):
    '''Returns the data with the new health '''
    x_2 = copy.deepcopy(x_1)
    last_recover = []
    last_sick = []
    # Update health of sick an recovering
    for ind in enumerate(x_1):
        if ind[1][1] == 'sick':
            x_2[ind[0]][1] = 'recovering'
            last_sick.append(x_2[ind[0]][0])
        elif ind[1][1] == 'recovering':
            x_2[ind[0]][1] = 'healthy'
            last_recover.append(x_2[ind[0]][0])
    # Update health of new sicks
    for ind in enumerate(in_common_1):
        current_common = ind[1]
        current_health = x_1[ind[0]][1]
        for ind_1 in enumerate(x_1):
            if in_common_1[ind_1[0]] == current_common:
                if current_health in ('sick', 'recovering'):
                    if x_1[ind_1[0]][1] == 'healthy':
                        x_2[ind_1[0]][1] = 'sick'
                        last_recover.append(x_2[ind[0]][0])
    return (x_2, last_recover, last_sick)


def refresh_cities(city_pointer, x_1):
    '''
    Return updated pointer of the cities
    '''
    x_2 = copy.deepcopy(city_pointer)
    for ind in enumerate(x_1):
        current_len = len(ind[1][2]) - 1
        if city_pointer[ind[0]] == current_len:
            x_2[ind[0]] = 0
        else:
            x_2[ind[0]] = city_pointer[ind[0]] + 1
    return x_2


def reset_recover():
    '''
    Returns a empty list
    '''
    lst = []
    return lst


def is_all_healthy(x_1):
    '''
    Return true is all people is healthy
    '''
    flag = True
    for ind in enumerate(x_1):
        if ind[1][1] != 'healthy':
            flag = False

    return flag


if __name__ == '__main__':
    X = []
    NUMBER = 0
    FILE = "/home/juanmusic1/training_1/code/codeabbey/214/DATA.lst"
    (X, NUMBER) = read_data(FILE)
    DAY = 0
    F_DAY = True
    ALL_HEALTHY = False
    CITY_POINTER = []
    IN_COMMON = []
    LAST_RECOVER = []
    LAST_SICK = []
    for i in range(0, len(X)):
        CITY_POINTER.append(0)
        IN_COMMON.append(0)

    # 100 days simulation
    while DAY < 100 and ALL_HEALTHY is not True:
        if F_DAY:
            # Get the people in the same place
            IN_COMMON = get_common(X, CITY_POINTER, NUMBER)
            # Update cities
            CITY_POINTER = refresh_cities(CITY_POINTER, X)
            # Set first day to false
            F_DAY = False
        else:
            # Refresh health
            LAST_RECOVER = reset_recover()
            LAST_SICK = reset_recover()
            (X, LAST_RECOVER, LAST_SICK) = refresh_health(X, IN_COMMON)
            ALL_HEALTHY = is_all_healthy(X)
            # Get the people in the same place
            IN_COMMON = get_common(X, CITY_POINTER, NUMBER)
            # Update cities
            CITY_POINTER = refresh_cities(CITY_POINTER, X)
        DAY = DAY + 1

    if ALL_HEALTHY:
        STRING = ""
        for ind_01 in enumerate(LAST_RECOVER):
            STRING = STRING + str(ind_01[1]) + " "
        STRING = STRING + str(DAY - 2)
        print STRING
    elif DAY > 100:
        STRING = ""
        for ind_01 in enumerate(LAST_SICK):
            STRING = STRING + str(ind_01[1]) + " "
        STRING = STRING + str(DAY - 2)
        print STRING

# $ python3 juanmusic1.py
# Max 7
