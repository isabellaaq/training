#!/usr/bin/env python2.7
'''
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
'''

GRAPH = []
NODES = EDGES = 0

with open('DATA.lst') as data:
    NODES, EDGES = data.readline().strip().split()
    for line in data:
        GRAPH.append(map(int, line.split()))

QUEUE = [0]
SEEN = {0: -1}

while QUEUE:
    CURRENT = QUEUE.pop(0)
    NEIGHBORS = []
    for edge in GRAPH:
        fetch = list(edge)
        if CURRENT in fetch:
            fetch.remove(CURRENT)
            if fetch[0] not in SEEN.keys():
                NEIGHBORS.append(fetch[0])
    NEIGHBORS.sort()
    QUEUE.extend(NEIGHBORS)
    SEEN.update(dict.fromkeys(NEIGHBORS, CURRENT))

print ' '.join(map(str, SEEN.values()))

# pylint: disable=pointless-string-statement

'''
$ python jicardona
-1 11 29 11 33 25 7  10 33 25 0  0  28 11 22 41 29 32 28 25 29 20
24 11 0  10 28 1  10 0  25 28 11 10 2  32 29 33 1  16 1  10 25 29
'''
