# $ rubocop jdanilo7.rb
# Inspecting 1 file

# 1 file inspected, no offenses detected

def read_lines_file(filename)
  lines = []
  File.foreach(filename) do |line|
    lines.push(line.split(' '))
  end
  lines
end

def read_words_file(filename)
  words = []
  File.foreach(filename) do |line|
    words.push(line)
  end
  words
end

def check_word(letters, word)
  matches = 1
  word.split('')[0...-1].each do |char|
    unless letters.index(char).nil?
      letters.delete_at(letters.index(char))
      next
    end
    matches = 0
    break
  end
  matches
end

def count_words(letters, words, length)
  word_count = 0
  words.each do |word|
    next if length != (word.length - 1)

    word_count += check_word(Array.new(letters), word)
  end
  word_count
end

def process_input(letters_lists, words)
  count_list = ''
  letters_lists.each do |list|
    length = list[0].to_i
    list.shift
    count_list += count_words(list, words, length).to_s + ' '
  end
  count_list
end

letters = read_lines_file('DATA.lst')
letters.shift
words = read_words_file('../127/words.lst')
puts process_input(letters, words)

# $ ruby jdanilo7.rb
# 1 8 4 17 8 6 1 3 9 37 9 8 6 7
