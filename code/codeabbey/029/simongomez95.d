/*
➜  029 git:(sgomezatfluid) ✗ dscanner -S sgomezatfluid.d && dscanner -s sgomezatfluid.d
➜  029 git:(sgomezatfluid) ✗ dmd sgomezatfluid.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;
import std.algorithm;
import std.array;

void main() {
  File file = File("DATA.lst", "r");
  file.readln();
  string[] numberstring = file.readln().split(" ");
  int[] numberarr = getNumberArr(numberstring);
  int[] orderedindex = [];
  string answer;

  for(int k=1; k <= numberarr.length; k++) {
    orderedindex = orderedindex ~ k;
  }

  for(int i=0; i < numberarr.length; i++) {
    for(int j=0; j < numberarr.length; j++) {
      if(numberarr[i] < numberarr[j]) {
        swap(numberarr[i], numberarr[j]);
        swap(orderedindex[i], orderedindex[j]);
      }
    }
  }

  foreach(int idx; orderedindex) {
    answer ~= to!string(idx) ~ " ";
  }
  writeln(answer);
  file.close();
}

private int[] decomposeInt(int value) {
  int[] decomposed = [];
  while (value > 0) {
    const int digit = value % 10;
    decomposed.insertInPlace(0, digit);
    value /= 10;
  }
  return decomposed;
}

private int[] getNumberArr(string[] numberstring) {
  int[] numberarr;
  foreach(string numstr; numberstring) {
    numberarr ~= to!int(stripws(numstr));
  }
  return numberarr;
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
➜  029 git:(sgomezatfluid) ✗ ./sgomezatfluid
1 2 4 3 6 20 11 19 16 17 5 14 8 12 15 9 10 13 7 18
*/
