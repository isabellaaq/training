#!/usr/bin/python3
# -*- coding:  utf-8 -*-
""" This script reads a list of test words, looks for anagrams of test words
    in a dictionary and prints the number of anagrams for each test word.
"""

# Every letter is assigned a prime number. Letters that ocurr more frequently
# (in english) are assigned smaller numbers.
D = {'e': 2, 't': 3, 'a': 5, 'o': 7, 'i': 11, 'n': 13, 's': 17,
     'r': 19, 'h': 23, 'd': 29, 'l': 31, 'u': 37, 'c': 41, 'm': 43, 'f': 47,
     'y': 53, 'w': 59, 'g': 61, 'p': 67, 'b': 71, 'v': 73, 'k': 79,
     'x': 83, 'q': 89, 'j': 97, 'z': 101}


def encode(word):
    """ Encodes a given word as product(pi^ni), where pi is the
    i-th prime number and ni is the number of ocurrences of the letter
    corresponding to the prime number in the given word, e.g. "leer"
    is 2e 1l 1r = (2^2)*(31^1)*(19^1).
    """
    code = 1
    for char in word:
        code *= D[char]
    return code


def main():
    """ Reads a list of test words, looks for anagrams of test words in
    a dictionary and prints the number of anagram for each test word.
    """
    # Read test words
    f_testw = open("DATA.lst", "r")
    cases = int(f_testw.readline())
    test_words = {}
    for i in range(cases):
        word = f_testw.readline().strip()
        test_words[encode(word)] = [i, 0]
    f_testw.close()
    # Compare test word codes against words in the dictionary and
    # update number of anagrams for test word accordingly.
    dictio = open("words.lst", "r")
    for line in dictio:
        code = encode(line.strip().lower())
        if code in test_words:
            test_words[code][1] += 1
    dictio.close()
    # Print output
    out = [-1]*cases
    for entry in test_words.values():
        out[entry[0]] = entry[1] - 1
    print out


main()
#
# pylint raballestasr.py
# No config file found, using default configuration
# ************* Module raballestasr
# C:  52, 0:  Final newline missing (missing-final-newline)
#
# ------------------------------------------------------------------
# Your code has been rated at 9.62/10 (previous run:  9.60/10, +0.02)
