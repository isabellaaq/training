/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function wordSwap(wordToChange, numberMovement) {
  if (numberMovement > 0) {
    const firstPart = wordToChange.slice(numberMovement);
    const secondPart = wordToChange.slice(0, numberMovement);
    const completeWord = firstPart.concat(secondPart);
    const outputWord = completeWord.join('');
    const wordPrint = process.stdout.write(`${ outputWord } `);
    return wordPrint;
  }
  const firstPart = wordToChange.slice((wordToChange.length + numberMovement));
  const secondPart = wordToChange.slice(0, (wordToChange.length +
    numberMovement));
  const completeWord = firstPart.concat(secondPart);
  const outputWord = completeWord.join('');
  const wordPrint = process.stdout.write(`${ outputWord } `);
  return wordPrint;
}

function wordProcess(element) {
  const inputSegment = element.split(' ');
  const numberMovement = parseInt(inputSegment[0], 10);
  const wordToChange = inputSegment[1].split('');
  const finalWord = wordSwap(wordToChange, numberMovement);
  return finalWord;
}


function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputSentences = inputFile.slice(1);
  const eachSentence = inputSentences.map((element) => wordProcess(element));
  return eachSentence;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    dataProcess(erro, contents));
}
/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:2
3 forwhomthebelltolls
-6 verycomplexnumber
-------------------------------------------------------------------
output:
whomthebelltollsfor numberverycomplex
*/
