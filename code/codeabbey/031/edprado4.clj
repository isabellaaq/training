(ns rotatestring.core
  (:gen-class))
(use '[clojure.string :only (split)])

(defn rotcadena [s n]
  (def cadena (split s #""))
  (if (> n 0)
    (dotimes [_ n]
      (def temp (cadena 0))
      (dotimes [i (count s)]
        (if (not= i (dec(count s))) (def cadena (assoc cadena  i (cadena (inc i))))))
      (def cadena (assoc cadena (dec (count s)) temp))))
  (if (< n 0)
    (dotimes [_ (* -1 n)]
      (def temp (cadena (dec (count cadena))))
      (loop [j (dec (count cadena))]
        (when (>= j 0)
          (if (not= j (dec (count s)))(def cadena (assoc cadena (inc j) (cadena j))))
          (recur (- j 1))))
      (def cadena (assoc cadena 0 temp))))
  
  (print (str (apply str cadena) " ")))

(defn -main []
  (def data ["tagwuflxgehodfta" "omuheyikieomqotglysdiw" "qnvgejurgupyoahyiujwhqcy" "zqytbgjhlsuuzylytfamouveo" "inzceswyyygdsdalomepseoa" "uxmpazjcxuywuxey" "vufzlxihyxzelrjordfuabm" "oppoiekoiwguiiux" "foziaodcouzycioqr"])
  (def index [5 3 -1 -2 7 -2 5 6 4])
  (dotimes [i (count data)]
   (rotcadena (data i) (index i))))
  
