/*
$ eslint tizcano.js
$
*/

function factorial(x, acum = 1) {
  return x ? factorial(x - 1, x * acum) : acum;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const solvedArray = inputFile
    .slice(1)
    .map((element) => {
      const inputElements = element.split(' ');
      return (
        factorial(inputElements[0]) /
        (factorial(inputElements[1]) *
          factorial(inputElements[0] - inputElements[1]))
      );
    })
    .join(' ');
  const output = process.stdout.write(`${ solvedArray }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
11919192480 21042072975 6358402050 49594720968 49280065120 56672074888
3679075400 27540584512
*/
