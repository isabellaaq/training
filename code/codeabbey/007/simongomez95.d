/*
$ dub run dscanner -- -S simongomez95.d
Building package dscanner in /Users/nekothecat/.dub/packages/dscanner-0.5.11/ds
canner/
Running pre-generate commands for dscanner...
Performing "debug" build using /Library/D/dmd/bin/dmd for x86_64.
stdx-allocator 2.77.2: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for configuration "unittest" is up to dat
e.
libdparse 0.9.8: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../../../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -S s
imongomez95.d

$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;
import std.algorithm;
import std.math;

void main() {
  File file = File("DATA.lst", "r");
  string numbers = file.readln();
  string[] numberstring = numbers.split(" ");
  string answer;
  float celsius, fahrenheit;
  for(int i=1; i<numberstring.length; i++) {
    fahrenheit = to!float(stripws(numberstring[i]));
    celsius = round((fahrenheit - 32) / 1.8);
    answer = answer ~ " " ~ to!string(celsius);
  }
  writeln(answer);
  file.close();
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*

$ ./simongomez95
  0 247 116 26 42 38 146 57 139 77 94 117 309 232 264 11 138 169 278 132 126
  263 293 138 262 123 170 298 154 11 121 154 257 237
*/
