#include <iostream>

using namespace std;

void graph_generator(int numv, int X0){
    int graph[numv][numv]={}, sum[numv]={};
    long int X_curr=X0;
    for(int i=0;i<numv;i++){
        int n=0;
        while(n!=2){
            int A=445;
            long int X1, X2, C=700001, M=2097152;
            X1=(X_curr*A+C)%M;
            X2=(X1*A+C)%M;
            X_curr=X2;
            X1=(X1%numv)+1; X2=(X2%numv)+1;
            if((graph[i][X1-1]==0)&&(i!=X1-1)){
                graph[i][X1-1]=1; graph[X1-1][i]=1;
                sum[i]+=X2; sum[X1-1]+=X2;
                n++;
                continue;
            }
            n++;
        }
    }
    for(int i=0;i<numv;i++){
        cout<<sum[i]<<' ';
    }
}

int main(void){
    int numv, X0;
    cin>>numv>>X0;
    graph_generator(numv,X0);
    return 0;
}
