/**
 * $ dartanalyzer --analysis_options.yaml < dferrans.dart # linting
 *  analysis_options.yaml
 *  (linter: rules:- annotate_overrides- - prefer_is_not_empty hash_and_equals)
 * $ No issues found!
 */
import 'dart:io';
import 'dart:math' as math;

void main() async {
 List<String> data = await new File( 'DATA.lst').readAsLines();
 var answer = [];
    for (var i = 0; i < data.length; i++) {
          if(i >0){
          var values = data[i].split(' ');
          var A = double.parse(values[1]);
          var D = double.parse(values[0]);
          var H = ( D * math.tan(( ( A  - 90.00) * (math.pi / 180.00))));
          answer.add(H.round());
          }
    }
print(answer.join(" "));
}
/**
 * $ dart dferrans.dart
 * $ 20 64 34 43 25 54 55 42 20 36 50 25 61 16
 **/
