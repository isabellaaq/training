/*
$ eslint tizcano.js
$
*/

function solver(lenghtA, lengthB, lengthC) {
  const calculatedHypo = Math.pow(lenghtA, 2) + Math.pow(lengthB, 2);
  const inputHypo = Math.pow(lengthC, 2);
  if (calculatedHypo > inputHypo) {
    return 'A';
  } else if (calculatedHypo < inputHypo) {
    return 'O';
  }
  return 'R';
}

function solverLink(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const inputArray = inputFile.slice(1);
  const solvedArray = inputArray.map((element) => {
    const inputLine = element.split(' ').map((elem) => parseInt(elem, 10));
    return solver(inputLine[0], inputLine[1], inputLine[2]);
  });
  const output = process.stdout.write(`${ solvedArray.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solverLink(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
O A A R R R R R O O A A O A R R R R O A
*/
