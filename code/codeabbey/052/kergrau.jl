#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA52.lst") do file
  flag = false
  answer = ""

  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end

    i = split(ln, " ")
    number1 = parse(Int64, i[1])
    number2 = parse(Int64, i[2])
    number3 = parse(Int64, i[3])
    hypo = sqrt((number1 ^ 2) + (number2 ^ 2))
    maxi = (maximum([number1, number2, number3]))
    if hypo > maxi
      println("A")
    elseif hypo < maxi
      println("O")
    else
      println("R")
    end
  end
end

# $kergrau.jl
# O A A R R R R R O O A A O A R R R R O A
