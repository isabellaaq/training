###
  $ coffeelint adrianfcn.coffee
    ✓ 172.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file

###
fs = require 'fs'

cloudAltitudeMeasurement = (inputs) ->
  A = inputs[1] * Math.PI / 180
  B = inputs[2] * Math.PI / 180
  letterH = Math.round((inputs[0]*Math.tan(A))/(1-(Math.tan(A)/Math.tan(B))))
  return letterH

solution = (dat) ->
  line = dat.split("\n")
  sol = ""
  for l in line
    values = []
    for v in l.split(" ")
      values.push(parseFloat(v, 10))
    if l.length > 2
      sol += cloudAltitudeMeasurement(values) + " "
  console.log(sol)

main = () ->
  fs.readFile('DATA.lst', (err, dat) -> solution(dat.toString()))

main()

###
  $ coffe adrianfcn.coffee
1207 1609 1163
###
