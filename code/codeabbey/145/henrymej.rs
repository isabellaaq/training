
/*
rustfmt henrymejia2.rs -f #format code instead of linting
rustc henrymejia2.rs #compilation
*/

use std::fs;

fn power(base: u64, exp: usize) -> u64 {
    let mut p = 1;
    let mut i = 0;
    while i < exp {
        p *= base;
        i += 1;
    }
    return p;
}

fn modprod(a: u64, b: u64, c: u64) -> u64 {
    return (a * b) % c;
}

fn main() {
    let filename = "DATA.lst";
    let string = fs::read_to_string(filename).expect(
      "Something went wrong reading the file");
    let split = string.split("\n");
    let vec = split.collect::<Vec<&str>>();
    let cases = vec[0].parse::<usize>().unwrap();
    for i in 1..(cases + 1) {
        let subvec = vec[i].split(" ").collect::<Vec<&str>>();
        let a = subvec[0].parse::<u64>().unwrap();
        let mut b = subvec[1].parse::<u64>().unwrap();
        let c = subvec[2].parse::<u64>().unwrap();
        let mut powers: [u64; 50] = [0; 50];
        let mut digits: [u64; 50] = [0; 50];
        powers[0] = a;
        for k in 1..50 {
            powers[k] = modprod(powers[k - 1], powers[k - 1], c);
        }
        for k in 0..50 {
            let index = 50 - k - 1;
            let p = power(2, index);
            digits[index] = b / p;
            b = b % p;
        }
        let mut result = 1;
        for k in 0..50 {
            if digits[k] == 1 {
                result = modprod(powers[k], result, c);
            }
        }
        println!("{}", result);
    }
}

/*
./henrymej
6415497
47633440
149944057
237692363
131543378
113801124
88101499
28612040
181689093
25323678
202282455
111408735
50002865
158080487
185441504
116231942
303675996
136537219
2306131
60703135
221267643
87243846
136815367
44720527
37729728
272546326
167240652
88399532
23848690
*/
