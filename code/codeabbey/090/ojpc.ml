let printArray arr = Array.iter (Printf.printf "%s") arr;;
let n_max = function
    [] -> invalid_arg "empty list"
  | x::xs -> List.fold_left max x xs ;;
let nperm array =
    let n = Array.length array in
    let i = let rec aux i = 
        if (i < 0) || (array.(i) < array.(i+1)) then i
        else aux (i - 1) in aux (n - 2) in
    let rec aux j k = if j < k then
        let t = array.(j) in
            array.(j) <- array.(k);
            array.(k) <- t;
            aux (j + 1) (k - 1)
    else () in aux (i + 1) (n - 1);
    if i < 0 then false else
        let j = let rec aux j =
            if array.(j) > array.(i) then j
            else aux (j + 1) in aux (i + 1) in
        let t = array.(i) in
            array.(i) <- array.(j);
            array.(j) <- t;
            true;;

let a = [|"A";"B";"C";"D";"E";"F";"G";"H";"I";"J";"K";"L";|];;
let j= ref 0;;
let cont= ref 0;;
let inp= List.sort compare [413748139;137146868;453034141;450637799;129877385;12190086;390246739;208267551;163687341;244873729;270662231;94801928;437523319;385373520;237745518;134717863;79232364;88996060;70341541;95111870;472144980;326626338];;
for i=0 to n_max inp do
        if i=List.nth inp (!j) then  (printArray a; print_string " "; j:= !j+1;) else j := !j;
        nperm a;
        cont:= !cont+ 1;
done;;
