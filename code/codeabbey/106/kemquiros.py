'''
 * Author: Kemquiros
 * Purpose: Solve the 106th CodeAbbey problem.
 * Language:  Python
'''

def speed_of_prime_mover(x, vector_velocity):
 sector = x // 20 
 if sector < 5:
  return vector_velocity[int(sector)]
 elif sector >=5:
  return 0 #The last sector :)

def end_of_simulation(x_driver, x_box, v_box):
 if x_driver >= 100:
  if abs(x_driver - x_box) <= 0.1 :
   if abs(v_box) <= 0.1 :
    return True
 else:
  return False

def simulation(vector_velocity):
 x = 0     # initial coordinate of the box
 v = 0     # initial speed of the box
 xd = 0    # initial coordinate of the mover (or driver)
 t = 0     # initial time
 dt = 0.2  # time step of simulation
 m = 5     # mass of the box
 k = 0.5   # coefficient of elasticity of the spring (as in Hooke's Law)
 b = -0.5  # coefficient of friction for the box (viscous friction, proportional to speed)
 while not end_of_simulation(xd, x, v) :
  force_spring = k * (xd - x)
  force_friction = b * v
  force_total = force_spring + force_friction
  acceleration = force_total / m
  x = x + v * dt
  v = v + acceleration * dt
  t = t + dt
  xd = xd + speed_of_prime_mover(xd, vector_velocity) * dt
 return t
  

def can_change_velocity(new_v, pos, vector):
 if pos == 4:
  if abs(vector[3] - new_v) <= 3:
   return True
  else:
   return False
 elif pos == 0:
  if abs(vector[1] - new_v) <= 3:
   return True
  else:
   return False
 else:
  if (abs(vector[pos-1] - new_v) <= 3) and (abs(vector[pos+1] - new_v) <= 3) :
   return True
  else:
   return False     

def transform(pair):
 pos, vel = 0 , 0
 if "+" in pair[1]:
  vel = float(pair[1][1:])
 elif "-" in pair[1]:
  vel = float(pair[1][1:]) * (-1)
 return int(pair[0]), vel
 
def mutation(vector_velocity, pos, vel):
  new_velocity = vector_velocity[pos] + vel
  if (pos == 0):
   if (new_velocity > 0) and  (new_velocity <= 3) :
    if can_change_velocity(new_velocity, pos, vector_velocity):
     vector_velocity[pos] = new_velocity
  elif (pos != 0) and (new_velocity > 0):
   if can_change_velocity(new_velocity, pos, vector_velocity):
    vector_velocity[pos] = new_velocity
#-------------------
#------ MAIN -------
#-------------------
opt = {}
n = int(raw_input())
vector_velocity = raw_input().strip().split(" ")

for i in range(0,len(vector_velocity)):
 vector_velocity[i] = int(vector_velocity[i])

for i in range(0,n):
 opt[i] =  raw_input().strip().split(" ")

best_time = simulation(vector_velocity)
for key in opt:
 val = opt[key]
 pos, vel = transform(val)
 current_vel = vector_velocity[pos]
 mutation(vector_velocity, pos, vel)
 stop_time = simulation(vector_velocity)
 if stop_time < best_time:
  best_time = stop_time
 else:
  vector_velocity[pos] = current_vel

resul = ""
for v in vector_velocity:
 if v.is_integer():
  v = int(v)
 resul = resul + str(v) + " "
resul = resul + str(best_time)
print(resul)
