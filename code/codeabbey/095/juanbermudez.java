package RETO095;

public class Regression {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    double[] xValues= {107,122,84,83,107,119,100,103,128,101,125,57,83,142,142,137,122,107,
                       97,132,131,102,57,61,61,149,112,125,115,145,55,104,64,68,51,69,91,67,
                       50,119,124,140,73,58,86,149,132,108,134,56,57,118,90,80,101,62,63,66,
                       147,67,125,123,116,118,148,106,115,113,120,135,127,69,53};
    double[] yValues= {290,314,274,324,364,368,309,336,354,339,358,209,257,414,394,380,337,316,
                       249,364,365,307,201,223,212,403,380,362,399,372,193,307,225,228,200,239,
                       282,205,188,336,357,435,244,207,273,397,328,318,371,228,215,344,262,235,
                       304,217,261,221,397,234,347,351,324,302,399,305,319,355,321,334,373,238,201};
    LinealValues linealValues=new LinealValues(xValues, yValues);
    System.out.println(linealValues.getB()+" "+linealValues.getC());
  }

}


class LinealValues{
  public LinealValues(double[] xValues,double[] yValues) {
    // TODO Auto-generated constructor stub
    this.xValues=xValues;
    this.yValues=yValues;
    N=xValues.length;
    SumXY=0;
    
    for (int i = 0; i < yValues.length; i++) {
      SumXY+=xValues[i]*yValues[i];
      medX+=xValues[i];
      medY+=yValues[i];
      X2+=Math.pow(xValues[i],2); 
    }
    medX=medX/N;
    medY=medY/N;
  }
  
  public double getB() {
    B=(SumXY-N*medX*medY)/(X2-N*Math.pow(medX, 2));
    C=medY-B*medX;
    return B;
  }
  

  public double getC() {
    return C;
  }


  private double[] xValues,yValues;
  private double SumXY,medX,medY,N,X2,B,C;
}
