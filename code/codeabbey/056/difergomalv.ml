let count table neighs =
  let x = ref 0 in
  for i=0 to ((Array.length neighs)-1) do
    if table.(neighs.(i)) ='X'then
      x:=!x+1
  done;
   !x
;;
let count_case1 table i= 
   if i=0 then
    let x = [| 1; 11; 12|] in
    count table x
  else if i=10 then
    let x = [| 9; 20; 21|] in
    count table x
  else
    let x = [| (i-1); (i+1); (i+10); (i+11); (i+12)|] in
    count table x
;;
let count_case2 table i= 
   if i=88 then
    let x = [| 89; 78; 77|] in
    count table x
  else if i=98 then
    let x = [|97; 87; 86|] in
    count table x
  else
    let x = [|(i-12); (i-11); (i-10); (i-1); (i+1)|] in
    count table x
;;
let count_case3 table i= 
   if (i mod 11) = 0 then
    let x = [|(i-11); (i-10); (i+1); (i+11); (i+12)|] in
    count table x
  else if ((i+1) mod 11) = 0 then
    let x = [|(i-12); (i-11); (i-1); (i+10); (i+11)|] in
    count table x
  else
    let x = [| (i-12); (i-11); (i-10); (i-1); (i+1); (i+10); (i+11); (i+12) |] in
    count table x
;;
let count_neighbors table i =
  if i <  11 then
    count_case1 table i
  else if i > 87 then
    count_case2 table i
  else
    count_case3 table i
;;
let check_born table i =
  let neighbors = count_neighbors table i in
  if neighbors = 3 then
    'X'
  else
    '-'
;;
let check_dies table i =
  let neighbors = count_neighbors table i in
  if neighbors = 2 || neighbors = 3 then
    'X'
  else
    '-'
;;

let check table i =
    if table.(i) = '-' then
      check_born table i
    else
      check_dies table i
;;

let print_table table =
  for i=0 to 8 do
    for f = 0 to 10 do
      print_char table.(f+(i*11))
    done;
    print_string "\n"
  done;
;;
let turno table =
let table_result = Array.make 99 '-'in
  for i = 0 to 98 do
    table_result.(i) <- check table i
  done;
  table_result
;;

let count_organisms table = 
let x = ref 0 in
  for i=0 to 8 do
    for f = 0 to 10 do
      if table.(f+(i*11)) = 'X' then
        x := !x+1
    done;
  done;
    print_int !x;
    print_string " ";
;;
let lines = Array.make 5 "";;
let table1 = Array.make 99 '-';;

for i = 0 to 4 do
  lines.(i) <- read_line ()
done;;

for i = 2 to 6 do 
  for f = 2 to 8 do
    table1.(f+(i*11)) <- lines.(i-2).[f-2];
  done;
done;;
let table2 = ref table1;;
for i = 0 to 4 do
  table2 := turno !table2;
   count_organisms !table2
done
