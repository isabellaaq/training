<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('./DATA.lst')) {
$file = file_get_contents("./DATA.lst");
$file = explode("\n", $file);
unset($file[0]);

$answer = array();
foreach ($file as $S) {
  $S = str_split($S);

    if ($S[0] == $S[3] OR $S[1] == $S[4] OR
      abs(ord($S[0]) - ord($S[3])) ==
      abs($S[1] - $S[4])) {
      $answer[] = 'Y';
    }
    else
      $answer[] = 'N';
}
echo join(' ', $answer);
}
else
  echo 'Error DATA.lst not found';
/*
./ciberstein.php
N N N Y N N N Y Y Y N Y Y Y Y N N N Y N N N Y N N
*/
?>
