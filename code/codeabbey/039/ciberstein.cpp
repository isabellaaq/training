/*
linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"
  -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\include"
  -I"C:\\...\c++" -L"C:\\...\lib" -static-libgcc

Compiling and linking using the "Dev-C++ 5.11"

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,30611324310303 MiB
- Compilation Time: 1,42s

/out:ciberstein.exe
*/
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

int main() {

ifstream fin ("./DATA.lst");

  if(!fin.fail()) {
    int nc, array[14];
    float X,op;
    char str[5];

    fin >> nc;

    for (int g = 0 ; g < nc ; g++) {
      fin >> str;

      for (int i=0; i<14; i++)
        fin >> array[i];
        X = op = 0;

      for (int i=0; i<14; i++)
        X += array[i];

      X /= 14;

      for (int i=0; i<14; i++)
        op += (X-array[i]) * (X-array[i]);

      op = sqrt(op/14);

      if (op > 0.04 * X)
        cout<< str << " ";
    }
  }
  else
    cout << "Error DATA.lst not found";

  return 0;
}
/*
$ ciberstein.exe
GOLD
*/
