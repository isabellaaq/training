; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kamadoatfluid.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kamadoatfluid.clj
  (:gen-class)
)

; tokenize whitespace separated string into a convenient type and container
(defn s2c [wsstr]
  (into [] (map #(Integer/parseInt %) (clojure.string/split wsstr #" ")))
)

; return N'th element on sequence [Xf = (A * Xi + C) % M]
(defn solve [A C M X0 N]
  (loop [i 0 x X0]
    (if (< i N)
      (recur (+ i 1) (mod (+ (* A x) C) M))
      x
    )
  )
)

; parse DATA.lst and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
              s  (count c)
              A  (get c 0)
              C  (get c 1)
              M  (get c 2)
              X0 (get c 3)
              N  (get c 4)]
          (if (= s 5)
            (print (str (solve A C M X0 N) " "))
          )
        )
      )
    )
    (println)
  )
)

; fire it all
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;   286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549 13339 6
