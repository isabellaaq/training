/*
$ eslint actiradob.js
$
*/

let front = [ [ '7', '8', '9' ], [ '4', '5', '6' ], [ '1', '2', '3' ] ];
let back = [ [ '-', '-', '-' ], [ '-', '-', '-' ], [ '-', '-', '-' ] ];
let left = [ [ '-', '-', '-' ], [ '-', '-', '-' ], [ '-', '-', '-' ] ];
let right = [ [ '-', '-', '-' ], [ '-', '-', '-' ], [ '-', '-', '-' ] ];
let upper = [ [ '-', '-', '-' ], [ '-', '-', '-' ], [ '-', '-', '-' ] ];
let down = [ [ '-', '-', '-' ], [ '-', '-', '-' ], [ '-', '-', '-' ] ];
const sides = {};

function moveFront() {
  const auxFrontA = [ front[2][0], front[1][0], front[0][0] ];
  const auxFrontB = [ front[2][1], front[1][1], front[0][1] ];
  const auxFrontC = [ front[2][2], front[1][2], front[0][2] ];
  const auxFront = [ auxFrontA, auxFrontB, auxFrontC ];
  const auxLeftA = [ down[0][0], down[0][1], down[0][2] ];
  const auxLeftB = [ left[1][0], left[1][1], left[1][2] ];
  const auxLeftC = [ left[2][0], left[2][1], left[2][2] ];
  const auxLeft = [ auxLeftA, auxLeftB, auxLeftC ];
  const auxRightA = [ upper[0][0], upper[0][1], upper[0][2] ];
  const auxRightB = [ right[1][0], right[1][1], right[1][2] ];
  const auxRightC = [ right[2][0], right[2][1], right[2][2] ];
  const auxRight = [ auxRightA, auxRightB, auxRightC ];
  const auxUpperA = [ left[0][0], left[0][1], left[0][2] ];
  const auxUpperB = [ upper[1][0], upper[1][1], upper[1][2] ];
  const auxUpperC = [ upper[2][0], upper[2][1], upper[2][2] ];
  const auxUpper = [ auxUpperA, auxUpperB, auxUpperC ];
  const auxDownA = [ right[0][0], right[0][1], right[0][2] ];
  const auxDownB = [ down[1][0], down[1][1], down[1][2] ];
  const auxDownC = [ down[2][0], down[2][1], down[2][2] ];
  const auxDown = [ auxDownA, auxDownB, auxDownC ];
  front = auxFront;
  left = auxLeft;
  right = auxRight;
  upper = auxUpper;
  down = auxDown;
}

function moveBack() {
  const auxBackA = [ back[2][0], back[1][0], back[0][0] ];
  const auxBackB = [ back[2][1], back[1][1], back[0][1] ];
  const auxBackC = [ back[2][2], back[1][2], back[0][2] ];
  const auxBack = [ auxBackA, auxBackB, auxBackC ];
  const auxLeftA = [ left[0][0], left[0][1], left[0][2] ];
  const auxLeftB = [ left[1][0], left[1][1], left[1][2] ];
  const auxLeftC = [ upper[2][0], upper[2][1], upper[2][2] ];
  const auxLeft = [ auxLeftA, auxLeftB, auxLeftC ];
  const auxRightA = [ right[0][0], right[0][1], right[0][2] ];
  const auxRightB = [ right[1][0], right[1][1], right[1][2] ];
  const auxRightC = [ down[2][0], down[2][1], down[2][2] ];
  const auxRight = [ auxRightA, auxRightB, auxRightC ];
  const auxUpperA = [ upper[0][0], upper[0][1], upper[0][2] ];
  const auxUpperB = [ upper[1][0], upper[1][1], upper[1][2] ];
  const auxUpperC = [ right[2][0], right[2][1], right[2][2] ];
  const auxUpper = [ auxUpperA, auxUpperB, auxUpperC ];
  const auxDownA = [ down[0][0], down[0][1], down[0][2] ];
  const auxDownB = [ down[1][0], down[1][1], down[1][2] ];
  const auxDownC = [ left[2][0], left[2][1], left[2][2] ];
  const auxDown = [ auxDownA, auxDownB, auxDownC ];
  back = auxBack;
  left = auxLeft;
  right = auxRight;
  upper = auxUpper;
  down = auxDown;
}

function moveLeft() {
  const auxLeftA = [ left[2][0], left[1][0], left[0][0] ];
  const auxLeftB = [ left[2][1], left[1][1], left[0][1] ];
  const auxLeftC = [ left[2][2], left[1][2], left[0][2] ];
  const auxLeft = [ auxLeftA, auxLeftB, auxLeftC ];
  const auxFrontA = [ upper[2][2], front[0][1], front[0][2] ];
  const auxFrontB = [ upper[1][2], front[1][1], front[1][2] ];
  const auxFrontC = [ upper[0][2], front[2][1], front[2][2] ];
  const auxFront = [ auxFrontA, auxFrontB, auxFrontC ];
  const auxBackA = [ back[0][0], back[0][1], down[2][0] ];
  const auxBackB = [ back[1][0], back[1][1], down[1][0] ];
  const auxBackC = [ back[2][0], back[2][1], down[0][0] ];
  const auxBack = [ auxBackA, auxBackB, auxBackC ];
  const auxUpperA = [ upper[0][0], upper[0][1], back[0][2] ];
  const auxUpperB = [ upper[1][0], upper[1][1], back[1][2] ];
  const auxUpperC = [ upper[2][0], upper[2][1], back[2][2] ];
  const auxUpper = [ auxUpperA, auxUpperB, auxUpperC ];
  const auxDownA = [ front[0][0], down[0][1], down[0][2] ];
  const auxDownB = [ front[1][0], down[1][1], down[1][2] ];
  const auxDownC = [ front[2][0], down[2][1], down[2][2] ];
  const auxDown = [ auxDownA, auxDownB, auxDownC ];
  left = auxLeft;
  front = auxFront;
  back = auxBack;
  upper = auxUpper;
  down = auxDown;
}

function moveRight() {
  const auxRightA = [ right[2][0], right[1][0], right[0][0] ];
  const auxRightB = [ right[2][1], right[1][1], right[0][1] ];
  const auxRightC = [ right[2][2], right[1][2], right[0][2] ];
  const auxRight = [ auxRightA, auxRightB, auxRightC ];
  const auxFrontA = [ front[0][0], front[0][1], down[0][2] ];
  const auxFrontB = [ front[1][0], front[1][1], down[1][2] ];
  const auxFrontC = [ front[2][0], front[2][1], down[2][2] ];
  const auxFront = [ auxFrontA, auxFrontB, auxFrontC ];
  const auxBackA = [ upper[0][0], back[0][1], back[0][2] ];
  const auxBackB = [ upper[1][0], back[1][1], back[1][2] ];
  const auxBackC = [ upper[2][0], back[2][1], back[2][2] ];
  const auxBack = [ auxBackA, auxBackB, auxBackC ];
  const auxUpperA = [ front[2][2], upper[0][1], upper[0][2] ];
  const auxUpperB = [ front[1][2], upper[1][1], upper[1][2] ];
  const auxUpperC = [ front[0][2], upper[2][1], upper[2][2] ];
  const auxUpper = [ auxUpperA, auxUpperB, auxUpperC ];
  const auxDownA = [ down[0][0], down[0][1], back[2][0] ];
  const auxDownB = [ down[1][0], down[1][1], back[1][0] ];
  const auxDownC = [ down[2][0], down[2][1], back[0][0] ];
  const auxDown = [ auxDownA, auxDownB, auxDownC ];
  right = auxRight;
  front = auxFront;
  back = auxBack;
  upper = auxUpper;
  down = auxDown;
}

function moveUpper() {
  const auxUpperA = [ upper[2][0], upper[1][0], upper[0][0] ];
  const auxUpperB = [ upper[2][1], upper[1][1], upper[0][1] ];
  const auxUpperC = [ upper[2][2], upper[1][2], upper[0][2] ];
  const auxUpper = [ auxUpperA, auxUpperB, auxUpperC ];
  const auxFrontA = [ right[0][2], right[1][2], right[2][2] ];
  const auxFrontB = [ front[1][0], front[1][1], front[1][2] ];
  const auxFrontC = [ front[2][0], front[2][1], front[2][2] ];
  const auxFront = [ auxFrontA, auxFrontB, auxFrontC ];
  const auxBackA = [ left[2][0], left[1][0], left[0][0] ];
  const auxBackB = [ back[1][0], back[1][1], back[1][2] ];
  const auxBackC = [ back[2][0], back[2][1], back[2][2] ];
  const auxBack = [ auxBackA, auxBackB, auxBackC ];
  const auxLeftA = [ front[0][2], left[0][1], left[0][2] ];
  const auxLeftB = [ front[0][1], left[1][1], left[1][2] ];
  const auxLeftC = [ front[0][0], left[2][1], left[2][2] ];
  const auxLeft = [ auxLeftA, auxLeftB, auxLeftC ];
  const auxRightA = [ right[0][0], right[0][1], back[0][0] ];
  const auxRightB = [ right[1][0], right[1][1], back[0][1] ];
  const auxRightC = [ right[2][0], right[2][1], back[0][2] ];
  const auxRight = [ auxRightA, auxRightB, auxRightC ];
  upper = auxUpper;
  front = auxFront;
  back = auxBack;
  left = auxLeft;
  right = auxRight;
}

function moveDown() {
  const auxDownA = [ down[2][0], down[1][0], down[0][0] ];
  const auxDownB = [ down[2][1], down[1][1], down[0][1] ];
  const auxDownC = [ down[2][2], down[1][2], down[0][2] ];
  const auxDown = [ auxDownA, auxDownB, auxDownC ];
  const auxFrontA = [ front[0][0], front[0][1], front[0][2] ];
  const auxFrontB = [ front[1][0], front[1][1], front[1][2] ];
  const auxFrontC = [ left[2][2], left[1][2], left[0][2] ];
  const auxFront = [ auxFrontA, auxFrontB, auxFrontC ];
  const auxBackA = [ back[0][0], back[0][1], back[0][2] ];
  const auxBackB = [ back[1][0], back[1][1], back[1][2] ];
  const auxBackC = [ right[0][0], right[1][0], right[2][0] ];
  const auxBack = [ auxBackA, auxBackB, auxBackC ];
  const auxLeftA = [ left[0][0], left[0][1], back[2][2] ];
  const auxLeftB = [ left[1][0], left[1][1], back[2][1] ];
  const auxLeftC = [ left[2][0], left[2][1], back[2][0] ];
  const auxLeft = [ auxLeftA, auxLeftB, auxLeftC ];
  const auxRightA = [ front[2][0], right[0][1], right[0][2] ];
  const auxRightB = [ front[2][1], right[1][1], right[1][2] ];
  const auxRightC = [ front[2][2], right[2][1], right[2][2] ];
  const auxRight = [ auxRightA, auxRightB, auxRightC ];
  down = auxDown;
  front = auxFront;
  back = auxBack;
  left = auxLeft;
  right = auxRight;
}

function move(instruction) {
  if (instruction === 'F') {
    moveFront();
  } else if (instruction === 'B') {
    moveBack();
  } else if (instruction === 'L') {
    moveLeft();
  } else if (instruction === 'R') {
    moveRight();
  } else if (instruction === 'U') {
    moveUpper();
  } else if (instruction === 'D') {
    moveDown();
  }
}

function search() {
  const rubik = [ front, back, left, right, upper, down ];
  const three = 3;
  const four = 4;
  const five = 5;
  rubik.map((side, index) => {
    side.map((row) => {
      row.map((piece) => {
        if (index === 0) {
          sides[piece] = 'F';
        } else if (index === 1) {
          sides[piece] = 'B';
        } else if (index === 2) {
          sides[piece] = 'L';
        } else if (index === three) {
          sides[piece] = 'R';
        } else if (index === four) {
          sides[piece] = 'U';
        } else if (index === five) {
          sides[piece] = 'D';
        }
        return piece;
      });
      return row;
    });
    return side;
  });
}

const fileS = require('fs');

function cube(miss, file) {
  let moves = file.split(/\r\n|\r|\n/g);
  let nMoves = 0;
  [ nMoves, moves ] = moves;
  moves = moves.split(' ');
  moves.map((inst) => move(inst));
  search();
  moves = nMoves;
  let answer = `${ sides['1'] } ${ sides['2'] } ${ sides['3'] } `;
  answer += `${ sides['4'] } ${ sides['5'] } ${ sides['6'] } `;
  answer += `${ sides['7'] } ${ sides['8'] } ${ sides['9'] }`;
  process.stdout.write(answer);
}

function fileLoad() {
  fileS.readFile('DATA.lst', 'utf8', (miss, file) => cube(miss, file));
}

fileLoad();

/*
$ node actiradob.js
F F D F F B L D U
*/
