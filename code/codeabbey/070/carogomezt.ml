let t = 900000;;
let x = ref (Scanf.scanf "%d\n"(fun x ->  x));;


let c = "bcdfghjklmnprstvwxz";;
let v = "aeiou";;
let pos = ref 0;;

let funny r c v = let word = String.make (Array.length r) 'a' in
                for i=0 to (Array.length r) - 1 do
                    if (i+1) mod 2 = 0 then
                    (
                        pos := r.(i) mod 5;
                        Bytes.set word i (Bytes.get v !pos)
                    )
                    else( pos := r.(i) mod 19;
                        Bytes.set word i (Bytes.get c !pos)
                        )
                done;
                word;;

let congruentialGenerator a c m n result = let j = ref 0 in
                                            let aux = ref 0 in
                                            while !n > 0 do
                                            aux := (a * !x + c) mod m;
                                            result.(!j) <- !aux;
                                            x := !aux ;
                                            n := !n - 1;
                                            j := !j + 1;
                                        done;
                                        result;;

let words = Hashtbl.create t;;

for i=0 to t - 1 do
    let n =  6 in
    let result = Array.make 6 0 in
    let r = congruentialGenerator 445 700001 2097152 (ref n) result in
    let z = funny r c v in
    let cont = ref 1 in
    if (Hashtbl.find_opt words z) = None then
    (
        Hashtbl.add words z !cont;
    )
    else (
        cont := Hashtbl.find words z;
        Hashtbl.add words z (!cont +1);
        )
done;;
let maxi = ref (-1);;
let result = ref "";;

Hashtbl.iter (fun key value -> if value > !maxi then
                                begin
                                    maxi := value;
                                    result := key;
                                end;) words;;
print_string !result;;
