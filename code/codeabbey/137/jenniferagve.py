"""
  Feature: Solving the challenge #137 Shannon-Fano Coding
  With Python v3
  From http://www.codeabbey.com/index/task_view/shannon-fano-coding


 Linting:   pylint jenniferagve.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def letter_counter(data):
    """Calculate the number of times that a letter is repeat and sorted them
    by value"""
    list1 = []
    uni_letters = set(data)
    for letter in uni_letters:
        binary = ""
        asc = ""
        quantity = data.count(letter)
        list1.append([letter, quantity, binary, asc])
    list1 = sorted(list1, key=lambda x: (-x[1], x[0]))
    return list1


def segmentation(list1):
    """Calculate the number where the array of string should be separated"""
    quantity = []
    length = len(list1)
    for i in range(0, length):
        quantity.append(list1[i][1])
    first_add = 0
    second_add = sum(quantity[0:len(quantity)])
    current_diff = abs(first_add - second_add)
    old_diff = (sum(quantity[0:len(quantity)])) + 1
    position = 0
    while old_diff > current_diff:
        old_diff = current_diff
        position += 1
        first_add = sum(quantity[0:position])
        second_add = sum(quantity[position:len(quantity)])
        current_diff = abs(first_add - second_add)
        if old_diff == current_diff:
            return position - 2
    return position - 2


def put_number(list1):
    """put the 1/I or 0/O depending on the side of the branch where is located
    the letter and the number of times it is repeated"""
    initial_position = segmentation(list1)
    for i in range(0, initial_position + 1, 1):
        list1[i][2] = list1[i][2] + "O"

    for i in range(initial_position + 1, len(list1), 1):
        list1[i][2] = list1[i][2] + "I"

    if (len(list1[0:initial_position + 1])) != 1:
        put_number(list1[0:initial_position + 1])
    if (len(list1[initial_position + 1:len(list1)])) != 1:
        put_number(list1[initial_position + 1:len(list1)])
    return list1


def put_asc(list1):
    "Put the ascii equivalent for each letter on the list"
    length = len(list1)
    for i in range(0, length):
        list1[i][3] = ord(list1[i][0])
    return list1


def total_answer(list1):
    """Print the ascii and equivalent I/O code after making the shannon-
    fano coding"""
    answer = ""
    for i in range(0, len(list1), 1):
        answer = answer + str(list1[i][3]) + " " + list1[i][2] + " "
    print answer


def init():
    """The main where each function it's called to calculate the shannon-fano
    coding"""
    data = open('DATA.lst', "r")
    data = data.readline()
    list1 = letter_counter(data)
    list1 = put_number(list1)
    list1 = put_asc(list1)
    total_answer(list1)


init()

# pylint: disable=pointless-string-statement

''' python jenniferagve.py
    input: of the freedom which it gives. I sent John, the coachman,
    to photograph, it is always under the honourable title of the woman.
     I had seen little of Holmes lately. My marriage had drifted us
     "Then, good-night, your Majesty, and I trust that we shall soon
     open drawers, as if the lady had hurriedly ransacked them before
    -------------------------------------------------------------------
    output: 32 OOO 101 OOI 116 OIO 97 OIIO 104 OIII 111 IOOO 114 IOOIO
    115 IOOII IOIO 110 IOIIO 105 IOIII 108 IIOOO 44 IIOOI 102 IIOIO
    121 IIOIIO 109 IIOIII 117 IIIOOO 103 IIIOOI 119 IIIOIO 99 IIIOII
    46 IIIIOOO 73 IIIIOOI 112 IIIIOIO 77 IIIIOII 98 IIIIIOO 34 IIIIIOIO
    45 IIIIIOIIO 72 IIIIIOIII 74 IIIIIIOO 84 IIIIIIOI 106 IIIIIIIO
    107 IIIIIIIIO 118 IIIIIIIII'''
