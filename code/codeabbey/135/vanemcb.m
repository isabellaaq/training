%mcc -m vanemcb.m

%Cell array that contents the code showed in the challenge
arrayCode={' ',[1 1];'t',[1 0 0 1];'n',[1 0 0 0 0];'s',[0 1 0 1];...
'r',[0 1 0 0 0];'d',[0 0 1 0 1];'!',[0 0 1 0 0 0];'c',[0 0 0 1 0 1];...
'm',[0 0 0 0 1 1];'g',[0 0 0 0 1 0 0];'b',[0 0 0 0 0 1 0];'v',...
[0 0 0 0 0 0 0 1];'k',[0 0 0 0 0 0 0 0 0 1];'q',[0 0 0 0 0 0 0 0 0 0 0 1];...
'e',[1 0 1];'o',[1 0 0 0 1];'a',[0 1 1];'i',[0 1 0 0 1];'h',[0 0 1 1];...
'l',[0 0 1 0 0 1];'u',[0 0 0 1 1];'f',[0 0 0 1 0 0];'p',[0 0 0 0 1 0 1];...
'w',[0 0 0 0 0 1 1];'y',[0 0 0 0 0 0 1];'j',[0 0 0 0 0 0 0 0 1];'x',...
[0 0 0 0 0 0 0 0 0 0 1];'z',[0 0 0 0 0 0 0 0 0 0 0 0]};

fileID = fopen('DATA.lst');
cellArrayText = textscan(fileID,'%s');
numWords = length(cellArrayText{1,1});
cellCodePhrase = cell(numWords,1);

%Counters to carrie out the concatenations of arrays
cont = 0;
cont2 = 1;
cont3 = 0;
cont4 = 1;
cont5 = 1;

%Cycles to asociate the input text with the code of "arrayCode"
for i=1:numWords
  vectorWord = cell2mat(cellArrayText{1,1}(i,1));
  cellCodeWord = cell(1,length(vectorWord));
    if i ~= numWords
      vectorWord(1,length(vectorWord)+1) = ' ';
    end
  for k=1:length(vectorWord)
    for v=1:length(arrayCode)
      if vectorWord(1,k) == arrayCode{v,1}
        cellCodeWord{1,k} = arrayCode{v,2};
        break
      end
    end
  end
  cont = cont + length(vectorWord);
  cellCodePhrase{i,1} = cellCodeWord;
end

%Cycles to create the vector withe binarie code
vectorCodePhrase = cell(1,cont);
for y=1:numWords
  vectorCodePhrase(1,cont2:cont3 + length(cellCodePhrase{y,1})) =...
  cellCodePhrase{y,1};
  cont2 = cont2 + length(cellCodePhrase{y,1});
  cont3 = cont3 + length(cellCodePhrase{y,1});
end
vectorCodePhrase = cell2mat(vectorCodePhrase);
sizeVector = length(vectorCodePhrase);
  if mod(sizeVector,8) ~= 0
    vectorCodePhrase(1,sizeVector+1:sizeVector+(8-mod(sizeVector,8))) = 0;
  end
sizeCell = length(vectorCodePhrase)/4;
cellHexCode = cell(sizeCell,1);
for z=1:sizeCell
  cellHexCode{z,1} = binaryVectorToHex(vectorCodePhrase(1,cont4:4*z));
  cont4 = (4*z) + 1;
end

%Cycle to create the final vector to show the hexadecimal code
sizeResult = sizeCell/2;
vectorResult = cell(1,sizeResult);
for x=1:sizeResult
  vectorResult{1,x} = cell2mat(cellHexCode(cont5:2*x,1))';
  cont5=(2*x)+1;
end
disp(vectorResult);

%vanemcb
%'93' 'B8' '51' '82' '25' '22' 'E1' '23' '12' 'E4' '1D' 'CD' 'AE' '24' '50'
%'09' '25' '2D' '86' '69' '5C' '80' '91' '01' 'A8' '84' '50' 'BC' 'C7' '0A'
%'D5' '72' '09' '60' 'D7' '88' '9A' '43' '0E' '95' '2E' '6E' '42' '28' '54'
%'55' 'B0' '97' '29' '8C' '34' 'C3' '93' 'B9' '04' 'D0' '89' '52' '5C' '8D'
%'17' '7A' '12' '10' 'E7' '4C' '2A' 'CA' '43' '60' '89' 'BC' 'C7' '93' 'A1'
%'EE' '05'  D3'  0C' '37' '00' '38' 'AD' '6A' 'F1' '13' '93' 'B8' '2B' '0A'
%'22' '67' '88' '99' '12' '84' 'B2' '70' '40' '72' '00' '23' '40' '1C' '81'
%'22' '8E' '50' 'E0' 'A1' '62' '89' '4C' '02' '63' '5C' '15' '03' '18' '17'
%'21' '6B' '50'
