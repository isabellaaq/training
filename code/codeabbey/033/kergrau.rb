# $ruby-lint kergrau.rb

answer = ''

File.open('DATA.lst', 'r') do |file|
  while line = file.gets

    line.split(' ').each do |num_as|
      character = num_as.to_i.to_s(2)
      residuum  = character.count "1"

      if (residuum % 2) == 0
        if num_as.to_i > 128
          answer << (num_as.to_i - 128).chr
        else
          answer << num_as.to_i.chr
        end
      end
    end
  end
end
puts answer

# ruby kergrau.rb
# 8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
