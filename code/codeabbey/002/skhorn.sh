#!/bin/bash
#
# Problem #2 Sum in Loop
#
while read -r line || [[ -n "$line" ]]; 
do
    len=$(echo -n "$line" | wc -c)
    if [[ $len -gt  3 ]]
    then
        arr=$(echo "$line" | tr " " "\n")
        for item in $arr;
        do
            let "sum += item"
        done
    fi    
    echo "$sum"

done < "$1"
