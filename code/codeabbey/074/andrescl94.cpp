#include <iostream>
#include <string>
#include <iomanip>
#include <math.h>

#define PI 3.14159265359
#define L_HOUR 6
#define L_MIN 9

using namespace std;

void coordinates(int num,int *h,int *m){
    for(int i=0;i<num;i++){
        for(int j=0;j<2;j++){
            int n=0;
            double X=1,Y=1,ang;
            switch(j){
                case 0: ang=30.0*(double(*(m+i))/60.0+double(*(h+i))); break;
                case 1: ang=6.0*double(*(m+i));
            }
            ang*=(PI/180.0);
            switch(j){
                case 0:X*=(10.0+L_HOUR*sin(ang));
                       Y*=(10.0+L_HOUR*cos(ang)); break;
                case 1:X*=(10.0+L_MIN*sin(ang));
                       Y*=(10.0+L_MIN*cos(ang));
            }
            cout<<fixed<<setprecision(8)<<X<<' '<<Y<<' ';
        }
    }
}

int main(void){
    int num;
    cin>>num;
    string time;
    int h[num],m[num];
    for(int i=0;i<num;i++){
        cin>>time;
        h[i]=stoi(time.substr(0,2));
        m[i]=stoi(time.substr(3,2));
    }
    coordinates(num,&h[0],&m[0]);
    return 0;
}
