/*
➜  015 git:(sgomezatfluid) ✗ dscanner -S sgomezatfluid.d
➜  015 git:(sgomezatfluid) ✗ dscanner -s sgomezatfluid.d
➜  015 git:(sgomezatfluid) ✗ dmd sgomezatfluid.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;
import std.algorithm;

void main() {
  File file = File("DATA.lst", "r");
  string[] numberstring = file.readln().split(" ");
  int max = to!int(numberstring[0]);
  int min = to!int(numberstring[0]);
  int current;
  string answer;

  for(int i=0; i<299; i++) {
    current = to!int(stripws(numberstring[i]));
    if(current > max) {
      max = current;
    }
    if(current < min) {
      min = current;
    }
  }
  answer = to!string(max) ~ " " ~ to!string(min);
  writeln(answer);
  file.close();
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
➜  015 git:(sgomezatfluid) ✗ ./sgomezatfluid
-78703 79241
*/
