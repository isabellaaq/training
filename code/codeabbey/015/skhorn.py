#!/usr/bin/env python3
"""
Problem #15 Maximum of array
"""
class MaximumOfArray:

    def __init__(self):

        while True:

            line = input()
            if line:
                test_case = line.split()
                self.maxd(test_case)
            else:
                break

    def maxd(self, array):
        maxi, mini = int(array[0]), int(array[0])
        for i, item in enumerate(array):
            item = int(item)
            if maxi > item:
                maxi = item
            elif mini < item:
                mini = item

        print("%s %s" %(mini, maxi))

MaximumOfArray()
