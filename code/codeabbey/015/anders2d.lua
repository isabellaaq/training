--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

require 'io'

local data
data = io.lines("DATA.lst")
local dataArr = {}

local function split(s, delimiter)
    local splitted = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(splitted, tonumber( match));
    end
    return splitted;
end



for line in data do
      local lineSplitted;
      dataArr=split(line," ")
end

local max;
local min;
max = dataArr[1]
min = dataArr[1]
for i = 1 ,#dataArr do
  if dataArr[i] >= max then
    max=dataArr[i]
  end
  if dataArr[i]<= min then
    min=dataArr[i]
  end
end

print(max .. " " .. min)
--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
79241 -78703
]]
