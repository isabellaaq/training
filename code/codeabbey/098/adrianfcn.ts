/*
$ tslint adrianfcn.ts
$ tsc adrianfcn.ts
*/
import * as fs from "fs";

function azimuthtoRad(azimuth) {
  return ((360 - azimuth) - 270) * (Math.PI / 180);
}

function input() {
  const data = fs.readFileSync("DATA.lst", "utf8");
  const arr = data.split("\n");
  arr.pop();
  arr.pop();
  arr.shift();
  const coordinates = [0, 0];
  for (const i of arr) {
    const v = i.split(" ");
    const feet = parseInt(v[1], 10);
    const azimuth = azimuthtoRad(parseInt(v[5], 10));
    coordinates[0] += feet * Math.cos(azimuth);
    coordinates[1] += feet * Math.sin(azimuth);
  }
  // tslint:disable-next-line:no-console
  console.log(Math.round(coordinates[0]), Math.round(coordinates[1]));
}

function main() {
  input();
}

main();

/*  $ node adrianfcn.js
  -190 2218
*/
