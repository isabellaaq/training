Program contarVocales;
uses crt;
type
     list = array [0 .. 5] of char;
var
    vocales : list;
    texto_in: string;
    j,t,lns,cont_vocales : integer;
begin
    vocales[0] := 'a';
    vocales[1] := 'e';
    vocales[2] := 'i';
    vocales[3] := 'o';
    vocales[4] := 'u';
    vocales[5] := 'y';

    writeln('================');
    readln(lns);

    for lns:=1 to lns do
    begin
        writeln('================');
        writeln('Introduzca texto');
        readln (texto_in);
        writeln(texto_in);
        j:=0;
        cont_vocales:=0;
        for j:= 0 to length(vocales)-1 do
        begin
            for t:=0 to length(texto_in) do
            begin
                if (vocales[j] = texto_in[t]) then
                    cont_vocales:=cont_vocales+1;
            end;
        end;
        writeln(cont_vocales);
    end;
end.
