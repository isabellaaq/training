;; $ lein eastwood
;;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;;   Directories scanned for source files:
;;     src test
;;   == Linting kamadoatfluid.clj ==
;;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

;; namespace
(ns kamadoatfluid.clj
  (:gen-class))

;; casts a string into a double
(defn todbl [s]
  (Double/parseDouble s))

;; square a number by long multiplication
(defn pow-2 [n]
  (* n n))

;; load lines of file handled by 'rdr' into a vector of double
(defn loadf [rdr]
  (vec (line-seq rdr)))

;; tokenize whitespace separated string into a list of double
(defn get-tokens-list [s]
  (map #(todbl %) (clojure.string/split s #" ")))

;; tokenize whitespace separated string into a vector of double
(defn get-tokens-vect [s]
  (vec (get-tokens-list s)))

;; returns the speed of the craft when reaching the planet surface
(defn get-reaching-speed [params _orders]
  ;; ==== Vars ===========================
  ;; planet
  ;;   R  = radius of planet        [   m]
  ;;   Go = gravity at surface      [g/s2]
  ;; rocket
  ;;   V  = speed of ship           [ m/s]
  ;;   Ve = speed of exhaust gases  [ m/s]
  ;;   Mc = mass of craft           [  kg]
  ;;   Mf = mass of fuel            [  kg]
  ;;   Br = fuel burning rate       [kg/s]
  ;; =====================================
  (with-local-vars [g      0.0
                    Br     0.0
                    dV     0.0
                    dMf    0.0
                    orders _orders

                    Mc     (get params 0)
                    Mf     (get params 1)
                    H      (get params 2)
                    V      (get params 3)]
    (while (> @H 0.0)
      (if (= (count @orders) 0)
        (var-set Br 0.0)
        (var-set Br (first @orders)))
      (var-set orders (rest @orders))
      (dotimes [i 1000]
        (if (> @H 0.0)
          (do
            (var-set H (- @H (* @V 0.01)))
            (var-set g (/ 4894411617020.0 (pow-2 (+ 1737100.0 @H))))
            (if (> @Mf 0.0)
              (do
                (var-set dMf (* @Br 0.01))
                (var-set Mf  (- @Mf @dMf))
                (var-set dV  (/ (* 2800.0 @dMf) (+ @Mf @Mc))))
              (do
                (var-set dMf 0.0)
                (var-set dV  0.0)))
            (var-set V (+ @V (- (* @g 0.01) @dV)))))))
    (int @V)))

;; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (let [f       (loadf rdr)
          params  (get-tokens-vect (get f 0))
          orders  (get-tokens-list (get f 1))]
      (println (get-reaching-speed params orders)))))

;; entry point
(defn -main [& args]
  (process_file "DATA.lst"))

;; $lein run
;; 273
