# > pylint kzccardona.py
#
#    -------------------------------------------------------------------
#    Your code has been rated at 10.00/10

"""
This code determine if a given number whether or not it is valid per
The Luhn formula.
"""
from __future__ import print_function


def valid(card_num):

    # type: (str) -> bool
    """
    This function apply the Luhn formula to the given number
    """
    try:
        character_list = ' '.join(list(card_num)).split()
        number_list = []
        for character in character_list:
            number_list.append(int(character))

        list_len = len(number_list)
        acumulated_sum = 0
        for position in range(list_len-1, -1, -1):
            if list_len > 1 and list_len % 2 == 0:
                if position % 2 == 0:
                    digit_number = int(number_list[position]) * 2
                    if digit_number > 9:
                        digit_number = digit_number - 9
                    acumulated_sum = acumulated_sum + digit_number
                else:
                    acumulated_sum = (acumulated_sum +
                                      int(number_list[position]))
            elif list_len > 1:
                if position % 2 > 0:
                    digit_number = int(number_list[position]) * 2
                    if digit_number > 9:
                        digit_number = digit_number - 9
                    acumulated_sum = acumulated_sum + digit_number
                else:
                    acumulated_sum = (acumulated_sum +
                                      int(number_list[position]))

        return True if acumulated_sum % 10 == 0 and list_len > 1 else False

    except ValueError:
        return False


for number in open('DATA.lst', 'r'):
    code = number.strip()
    print(valid(code))


# > python ./kzccardona.py
# True
# False
# False
