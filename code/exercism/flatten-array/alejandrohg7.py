#!/usr/bin/env python3
'''
$pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file'''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for line in opened_file:
            list_of_lists.append(line.split())
        return list_of_lists


def list_flattener(iterable):
    '''This function flatten a list'''
    filter_list = str(iterable).replace("[", "").replace("]", "")
    filter_list = filter_list.replace("None", "").replace("null", "")
    filter_list = filter_list.replace("'", "").replace(",", "")
    filter_list = filter_list.replace(")", "").replace("(", "")
    clean_list = ''.join(filter_list).split()
    final_list = []
    for number in clean_list:
        if '"' in number:
            final_list.append(str(number.replace('"', "")))
        else:
            final_list.append(int(number))
    return final_list


for each_list in open_file():
    print(list_flattener(each_list))
# $ python3 alejandrohg7.py
# [0, 1, 2]
# [1, 2, 3, 4, 5, 6, 7, 8]
# [0, 2, 2, 3, 8, 100, 4, 50, -2]
# [1, 2, 3, 4, 5, 6, 7, 8]
# [0, 2, 2, 3, 8, 100, -2]
# []
# []
# ['0', '1', '2']
