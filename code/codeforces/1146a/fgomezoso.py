# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the longest string's length when more than
a half of the characters are "a"s.
"""
from __future__ import print_function


def main():

    """
    Returns new_length as the longest string's length
    total_a must be greater than total_others
    """

    data = open("DATA.lst", "r")

    string = data.readline().rstrip('\n')
    length = len(string)
    total_a = 0
    total_others = 0

    for i in range(0, length):
        if string[i] == "a":
            total_a += 1
        else:
            total_others += 1

    while total_others >= total_a:
        total_others -= 1

    new_length = total_a + total_others

    print(new_length)

    data.close()


main()

# $ python fgomezoso.py
# 9
