# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the minimum possible equal price of all n goods in a list
so if you sell them for this price you will receive at least the same
(or greater) amount of money as if you sell them for their initial prices.
"""
from __future__ import print_function


def main():

    """
    The program must answer q independent queries. In every query, it computes
    the price average of a list and rounds the result.

    If the rounded average is'nt greater o equal to the initial total price
    then it adds 1 to the rounded average.
    """

    data = open("DATA.lst", "r")

    number_of_queries = int(data.readline().rstrip('\n'))
    result = []

    for k in range(number_of_queries):

        number_of_goods = int(data.readline().rstrip('\n'))
        input_prices = data.readline().rstrip('\n')
        price_list = list(map(int, input_prices.split()))

        add = 0

        for i in range(number_of_goods):

            add += price_list[i]

        average = add/number_of_goods
        r_average = round(average)

        if r_average*number_of_goods < add:
            r_average += 1

        result.append(r_average)

    for k in range(number_of_queries):

        print(result[k])

    data.close()


main()

# $ python fgomezoso.py
# 4672070
# 40051
# 387
