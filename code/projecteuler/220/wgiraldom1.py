"""
    I must admit: this problem was hard as fuck.
    >pylint wgiraldom1.py

    -------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 9.86/10, +0.14)
"""
import math

ROTATIONS = [[-1, 0], [0, 1], [1, 0], [0, -1]]
POINT = [0, 1, 1]
F_STORED_PATHS = [0, [0, 1, 1]]


def get_distance(p_a, p_b):
    """
        This function will calculate the distance
        from any given point, to another given point
        BUT on the plane made by its orientation-vector
        and its perpendicular.
    """
    parallel = ROTATIONS[p_a[2]]
    perpendicular = ROTATIONS[(p_a[2]+1) % 4]
    denominator = perpendicular[1]*parallel[0] - perpendicular[0]*parallel[1]
    diff = [p_b[0]-p_a[0], p_b[1]-p_a[1], (p_b[2]-p_a[2]) % 4]
    a_1 = perpendicular[1]*diff[0] - perpendicular[0]*diff[1]
    b_1 = diff[1]*parallel[0] - diff[0]*parallel[1]
    return (int(a_1/denominator), int(b_1/denominator), diff[2])


def turn_right(p_1):
    """
        The dolphin step
        (Sorry, my equatorial idol)
    """
    n_p = [p_1[0], p_1[1], p_1[2]]
    n_p[2] = (n_p[2]+1) % 4
    n_p[0] += ROTATIONS[n_p[2]][0]
    n_p[1] += ROTATIONS[n_p[2]][1]
    return n_p


def turn_left(p_1):
    """
        The dolphin step, but to the left
    """
    n_p = [p_1[0], p_1[1], p_1[2]]
    n_p[2] = (n_p[2]-1) % 4
    n_p[0] += ROTATIONS[n_p[2]][0]
    n_p[1] += ROTATIONS[n_p[2]][1]
    return n_p


def f_p(p_1, i):
    """
        This function complements f(p, i)
        for any given P and i.
    """
    if i == 1:
        return turn_left(p_1)
    n_p = f_main(p_1, i-1)
    n_p = turn_left(n_p)
    return f_p(n_p, i-1)


def apply_distance(p_1, p_2):
    """
        You know; the inverse of the get_distance function.
        We need to apply an offset to a given point
        respecting its orientation vector and its position
    """
    parallel = ROTATIONS[p_1[2]]
    perpendicular = ROTATIONS[(p_1[2]+1) % 4]
    n_p = p_1
    n_p[0] += p_2[0]*parallel[0] + p_2[1]*perpendicular[0]
    n_p[1] += p_2[0]*parallel[1] + p_2[1]*perpendicular[1]
    n_p[2] = (n_p[2]+p_2[2]) % 4
    return n_p


def f_main(p_1, i):
    """
        This is where magic happens (part 1)
        We have restricted memory and computational power,
        so we can't just make pasitos del delf&iacute;n indefinitely,
        we need to store the paths we've already done.

        In the church of computer science, greed is a capital sin.
    """
    if i < len(F_STORED_PATHS):
        return apply_distance(p_1, F_STORED_PATHS[i])
    n_p = [p_1[0], p_1[1], p_1[2]]
    n_p = f_main(n_p, i-1)
    n_p = f_main(n_p, 1)
    n_p = f_p(n_p, i-1)

    d_1 = get_distance(p_1, n_p)
    F_STORED_PATHS.append(d_1)
    return n_p


def move_steps(steps, i, p_1, direction):
    """
        This is where the magic happens (part 2)
        A recurrence of a recurrence.
        Now we just convert big paths into smaller ones,
        so each time we are nearer to our desired step and
        position.
    """
    n_p = [p_1[0], p_1[1], p_1[2]]
    length = 2**i
    if steps == length - 1:
        return f_main(n_p, i)
    if steps == 0:
        return p_1
    half = length/2 - 1
    if steps > half:
        n_p = f_main(n_p, i-1)
        if direction == "":
            n_p = turn_right(n_p)
        elif direction == "N":
            n_p = turn_left(n_p)

        return move_steps(steps-half-1, i-1, n_p, "N")
    return move_steps(steps, i-1, n_p, "")


def move(steps):
    """
        Now we just expose a convenient function which
        does not let the user know our painstakingly
        effort modelling this problem mathematically.
    """
    i = int(math.ceil(math.log(steps, 2)))
    return move_steps(steps-1, i, POINT, "")


FILE = open("DATA.lst")
STEPS = int(FILE.readline())
print move(STEPS)
#
#    > python wgiraldom1.py
#    [139776, 963904, 0]
