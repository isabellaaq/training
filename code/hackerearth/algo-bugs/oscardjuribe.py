# $ pylint oscardjuribe.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to solve the Bugs problem from hackerearth
"""


def binary_insert(value, array):
    """
    method to insert value using binary search
    """
    # minimum value
    min_array = 0
    # max value
    max_array = len(array)-1

    # iterate while i<size and value
    while min_array <= max_array:
        # mid position
        mid = (min_array+max_array)/2

        # if value is min_arrayer then move to right part of vector
        if value <= array[mid]:
            min_array = mid+1
        # if value is bigger then move to left part of vector
        elif value >= array[mid]:
            max_array = mid-1
        # position of the value
        # insert at that position
        else:
            return mid

    # if min_array is at the end then insert at last item
    if min_array == len(array):
        return len(array)
    #  if array at min_array position is smaller insert here
    elif array[min_array] < value:
        return min_array
    # else one more position
    return min_array+1


# number of testcases
TESTCASES = int(raw_input())

# empty array of bugs
BUGS_ARRAY = []

# solutions array
SOLUTIONS = []

for i in range(0, TESTCASES):
    # read line
    line = raw_input()

    # split line to get values
    splitted = line.split(" ")

    # check type of operation
    if splitted[0] == "1":
        current_value = int(splitted[1])
        pos = binary_insert(current_value, BUGS_ARRAY)
        BUGS_ARRAY.insert(pos, current_value)
    else:
        # calculate nth if size >=3
        if len(BUGS_ARRAY) >= 3:
            n = int(len(BUGS_ARRAY)/3)
            SOLUTIONS.append(BUGS_ARRAY[n-1])
        else:
            SOLUTIONS.append("Not enough enemies")

# print solution
for sol in SOLUTIONS:
    print sol
# cat DATA.lst | python oscardjuribe.py
# Not enough enemies
# 9
# 9
