/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;

public class lekriss {

  public static void main(String[] args) {
    try {

      FileReader file = new FileReader("DATA.lst");
      BufferedReader reader = new BufferedReader(file);
      String toModify = reader.readLine();
      String[] comparable = reader.readLine().split(" ");
      ArrayList<Integer> comparab = new ArrayList<Integer>();
      for(int i=0; i<comparable.length; i++)
        comparab.add(Integer.parseInt(comparable[i]));
      String control = "";

      for(int i=0; i<toModify.length(); i++) {
        if(i==comparab.get(0)-1||i==comparab.get(1)-1) {
          String h = toModify.charAt(i) + "";
            control += h.toUpperCase();
        }
        else {
          control += toModify.charAt(i);
        }
      }
      System.out.println(control);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
}
/*
$ java lekriss
$ Andres Felipe Lotaro
*/
