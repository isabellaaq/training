//De 1 a n

import java.util.Scanner;
 
public class Reto8 {
    public static void main(String[] args) {

        Scanner digit = new Scanner(System.in);
        int numero    = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto8*");
        System.out.println("*******");

        System.out.println("");
        System.out.println("***************************");
        System.out.println("*Serie de numeros de 1 a n*");
        System.out.println("***************************");
        System.out.println("");

        System.out.println(""); 
        System.out.print("Por favor digite un numero entero positivo: ");
        numero = digit.nextInt();
        System.out.println("");

        for (int i = 1; i<=numero; i++) {
            if (i<numero) {
                System.out.print(i+",");
            } 
            else {
                System.out.print(i);
            }
        }
    }
}
