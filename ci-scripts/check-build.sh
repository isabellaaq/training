#!/usr/bin/env sh

# This scripts checks whether the docker build image was changed and builds
# it if the branch is master

echo "${CI_REGISTRY_PASSWORD}" | docker login registry.gitlab.com \
  -u "${CI_REGISTRY_USER}" --password-stdin;

docker pull registry.gitlab.com/autonomicmind/training:builder;

docker build --cache-from registry.gitlab.com/autonomicmind/training:builder \
  -t registry.gitlab.com/autonomicmind/training:builder builder/;

if [ "$CI_COMMIT_REF_NAME" = "master" ]; then
  docker push registry.gitlab.com/autonomicmind/training:builder;
fi
